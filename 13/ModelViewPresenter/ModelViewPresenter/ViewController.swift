//
//  ViewController.swift
//  ModelViewPresenter
//
//  Created by Andras Somlai
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textInputField: UITextField!
    @IBOutlet weak var feedbackLabel: LabelView!
    let dataModel = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func updateUI() {
        view.endEditing(true) //billzet elrejtése
        textInputField.text = nil
        feedbackLabel.text = dataModel.enteredText
    }

    @IBAction func doneButtonPressed(_ sender: Any) {
        dataModel.enteredText = textInputField.text
        updateUI()
    }
}

