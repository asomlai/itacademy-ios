//
//  ViewController.swift
//  scrollDemo
//
//  Created by Andras Somlai on 2018. 04. 26..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let textField = UITextField()
        view.addSubview(textField)
        textField.frame = CGRect(x:0, y:50, width:200, height:40)
        textField.backgroundColor = UIColor(red: 123.0 / 255.0, green: 210.0 / 255.0, blue: 23.0/255.0, alpha: 1)
        textField.borderStyle = .bezel
        textField.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        scrollView.isPagingEnabled = true
        
        let numberOfPages = 3
        
        for i in stride(from: 0, to: numberOfPages, by: 1) {
            let newView = UIView()
            scrollView.addSubview(newView)
            newView.frame = CGRect(x:10 + (scrollView.frame.width*CGFloat(i)),
                                   y:10,
                                   width:scrollView.frame.size.width-20,
                                   height:scrollView.frame.size.height-20)
            newView.backgroundColor = UIColor.orange
        }
        
        //scrollView.contentSize = CGSize(width: 0, height: 0)
        scrollView.contentSize = CGSize(width: CGFloat(numberOfPages) * scrollView.frame.width,
                                        height: scrollView.frame.height + 10)
        //scrollView.isPagingEnabled = true
        
        scrollView.delegate = self
        
        
        
        
//        let myFrame = CGRect(x:0, y:0, width:100, height:100)
//        let mySize = CGSize(width:100, height:100)
//        let myPoint = CGPoint(x:0, y:0)
//        let simple : CGFloat = 43

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        debugPrint("scrollViewDidScroll")
        print("contentOffset: \(scrollView.contentOffset)")
        print("poage: = \(Int(scrollView.contentOffset.x/scrollView.frame.size.width))")
    }

//    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        debugPrint("scrollViewWillBeginDragging")
//    }
//
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//        debugPrint("scrollViewWillBeginDecelerating")
//    }
//
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //debugPrint("scrollViewDidEndDecelerating")
        scrollView.setContentOffset(CGPoint(x:100, y:0), animated: true)
    }
//
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        debugPrint("scrollViewWillEndDragging")
//    }
//
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        debugPrint("scrollViewDidEndDragging")
//    }

    
    
    //TextField delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        debugPrint("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        debugPrint("textFieldDidEndEditing")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        debugPrint("textFieldShouldReturn")
        self.view.endEditing(true)
        return true
    }
    
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        return true
//    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        debugPrint("shouldChangeCharactersIn")
//        if(string.lowercased() == "r") {
//            return true
//        }
//
//        return false
//    }
}

