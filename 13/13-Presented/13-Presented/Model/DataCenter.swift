//
//  DataCenter.swift
//  13-Presented
//
//  Created by Andras Somlai on 2018. 10. 30..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class DataCenter {
    //Singletonná tettük
    static let sharedInstance = DataCenter()
    private init(){}
    
    //Tárolt propertyk
    var name : String?
    var weight : Double?
    var height : Double?
    
}
