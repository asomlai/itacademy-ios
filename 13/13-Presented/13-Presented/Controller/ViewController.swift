//
//  ViewController.swift
//  13-Presented
//
//  Created by Andras Somlai on 2018. 10. 30..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tf1.placeholder = "név"
        tf2.placeholder = "testsúly"
    }

    @IBAction func save(_ sender: Any) {
        DataCenter.sharedInstance.name = tf1.text
        DataCenter.sharedInstance.weight = Double(tf2.text ?? "")
    }
    
}

