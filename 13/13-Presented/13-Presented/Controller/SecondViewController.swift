//
//  SecondViewController.swift
//  13-Presented
//
//  Created by Andras Somlai on 2018. 10. 30..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var tf1: UITextField!
    @IBOutlet weak var tf2: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tf1.placeholder = "magasság"
        tf2.placeholder = "testsúly"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        tf1.text = String(DataCenter.sharedInstance.height ?? 0)
        tf2.text = String(DataCenter.sharedInstance.weight ?? 0)
    }
    
    @IBAction func save(_ sender: Any) {
        DataCenter.sharedInstance.height = Double(tf1.text ?? "")
        DataCenter.sharedInstance.weight = Double(tf2.text ?? "")
    }
}
