//
//  FruitView.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class FruitView: UIView, UIScrollViewDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var likeScrollView: UIScrollView!
    @IBOutlet weak var oftenScrollView: UIScrollView!
    
    var parentViewController : ViewController!
    var dataModel : FruitViewModel! {
        didSet {
            imageView.image = UIImage(named: dataModel.fruit.rawValue)
            updateScrollViewDatas()
            updateBackgroundColor()
        }
    }
    
    @IBAction func addComment(_ sender: UIButton) {
        parentViewController.openCommentViewController(page: self.tag)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLikeScrollView()
        setupOftenScrollView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateOftenScrollViewFrames()
        updateLikeScrollViewFrames()
        updateScrollViewDatas()
    }
    
    func setupLikeScrollView() {
        for (index, actualRating) in Rating.allValues.enumerated() {
            let rateButton = UIButton()
            let buttonImage = UIImage(named: actualRating.rawValue)
            rateButton.setImage(buttonImage, for: .normal)
            likeScrollView.addSubview(rateButton)
            rateButton.tag = index
            rateButton.isUserInteractionEnabled = false
        }
        likeScrollView.isPagingEnabled = true
        likeScrollView.delegate = self
    }
    
    func setupOftenScrollView() {
        for (index, often) in Often.allValues.enumerated() {
            let label = UILabel()
            label.text = often.rawValue
            label.tag = index
            label.font = UIFont(name: "Arial-BoldMT", size: 22)!
            
            oftenScrollView.addSubview(label)
        }
        oftenScrollView.isPagingEnabled = true
        oftenScrollView.delegate = self
    }
    
    func updateOftenScrollViewFrames() {
        for label in oftenScrollView.subviews {
            label.frame = CGRect(x:CGFloat(label.tag)*oftenScrollView.frame.width,
                                 y:0,
                                 width:oftenScrollView.frame.width,
                                 height:oftenScrollView.frame.height)
        }
        
        oftenScrollView.contentSize = CGSize(width:CGFloat(Often.allValues.count) * oftenScrollView.frame.width,
                                        height:oftenScrollView.frame.height)
    }
    
    func updateLikeScrollViewFrames() {
        for button in likeScrollView.subviews {
            button.frame = CGRect(x:0,
                                  y:CGFloat(button.tag)*likeScrollView.frame.height,
                                  width:likeScrollView.frame.width,
                                  height:likeScrollView.frame.height)
        }
        
        likeScrollView.contentSize = CGSize(width:likeScrollView.frame.width, height:CGFloat(Rating.allValues.count)*likeScrollView.frame.width)
    }
    
    func updateScrollViewDatas() {
        let likeScrollPage = getScrollPage(for: dataModel.rating)
        let oftenScrollPage = getScrollPage(for: dataModel.often)
        
        likeScrollView.contentOffset = CGPoint(x:0 ,y:CGFloat(likeScrollPage)*likeScrollView.frame.height)
        
        oftenScrollView.contentOffset = CGPoint(x:CGFloat(oftenScrollPage)*oftenScrollView.frame.width, y:0)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if(scrollView == likeScrollView) {
            let page = Int(scrollView.contentOffset.y / scrollView.frame.height)
            switch page {
            case 0: dataModel.rating = .like
            case 1: dataModel.rating = .neutral
            case 2: dataModel.rating = .dislike
            default: break
            }
            updateBackgroundColor()
        }
        else if(scrollView == oftenScrollView) {
            let page = Int(scrollView.contentOffset.x / scrollView.frame.width)
            dataModel.often = getOften(for: page)!
        }
        
        parentViewController.dataManager.save()
    }
    
    private func getOften(for page : Int) -> Often? {
        switch page {
        case 0: return .day
        case 1: return .week
        case 2: return .twoWeek
        case 3: return .month
        case 4: return .quarterYear
        case 5: return .halfYear
        case 6: return .year
        default: return nil
        }
    }
    
    private func getScrollPage(for often : Often) -> Int {
        switch often {
        case .day: return 0
        case .week: return 1
        case .twoWeek: return 2
        case .month: return 3
        case .quarterYear: return 4
        case .halfYear: return 5
        case .year: return 6
        }
    }
    
    private func getScrollPage(for rating : Rating) -> Int {
        switch rating {
        case .like: return 0
        case .neutral: return 1
        case .dislike: return 2
        }
    }
    
    private func updateBackgroundColor() {
        switch dataModel.rating {
        case .like: self.backgroundColor = UIColor(red: 46.0/255.0,
                                               green: 204.0/255.0,
                                               blue: 113.0/255.0,
                                               alpha: 1.0)
        case .neutral: self.backgroundColor =  UIColor(red: 241.0/255.0,
                                                green: 196.0/255.0,
                                                blue: 15.0/255.0,
                                                alpha: 1.0)
        case .dislike: self.backgroundColor =  UIColor(red: 231.0/255.0,
                                                green: 76.0/255.0,
                                                blue: 60.0/255.0,
                                                alpha: 1.0)
        }
    }
}
