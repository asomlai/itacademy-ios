//
//  ViewController.swift
//  ScrollViewZoom
//
//  Created by Andras Somlai
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var upperScrollView: UIScrollView!
    @IBOutlet weak var lowerScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Kép nagyítása
        upperScrollView.delegate = self
        upperScrollView.minimumZoomScale = 0.5
        upperScrollView.maximumZoomScale = 10
        upperScrollView.tag = 2000
        
        let image = UIImage(named: "AppleHeadquarter")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
        upperScrollView.addSubview(imageView)
        
        
        //View nagyítása, amihez van hozzáadva subview
        lowerScrollView.delegate = self
        lowerScrollView.minimumZoomScale = 1
        lowerScrollView.maximumZoomScale = 10
        lowerScrollView.tag = 50000
        
        let containerView = UIView()
        let label = UILabel()
        label.text = "Hello, type your name"
        label.tag = 1
        containerView.addSubview(label)
        
        let textField = UITextField()
        textField.placeholder = "here"
        textField.tag = 2
        containerView.addSubview(textField)
        
        lowerScrollView.addSubview(containerView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //kép frame beállítása
        upperScrollView.subviews[0].frame = CGRect(x:0,
                                                   y:0,
                                                   width:upperScrollView.frame.width,
                                                   height:upperScrollView.frame.height)//upperScrollView.bounds
        
        
        
        //View, és a hozzá tartozó subview framek beálítása
        let containerView = lowerScrollView.subviews[0]
        containerView.frame = lowerScrollView.bounds
        
        let label = containerView.viewWithTag(1)!
        label.frame = CGRect(x:0,y:0,width:label.superview!.frame.width,height:30)
        
        let textField = containerView.viewWithTag(2)!
        textField.frame = CGRect(x:0,y:40 ,width:textField.superview!.frame.width,height:30)
    }

    //UISCROLLVIEW DELEGATE METÓDUSOK
    //Zoomoláshoz szükséges ezt a függvényt implementálni
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        //Mindig az 1 darab scrollView subView-t szeretnénk nagyítani
        return scrollView.subviews[0]
    }
    
    //Billzet eltüntetése scroll hatására
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        print("paraméer scrollview tag: \(scrollView.tag)")
        if(scrollView.tag == 50000) {
            view.endEditing(true)
        }
        
    }
}

