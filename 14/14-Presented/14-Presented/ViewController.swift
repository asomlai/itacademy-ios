//
//  ViewController.swift
//  14-Presented
//
//  Created by Andras Somlai on 2018. 11. 06..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myView0 = UIView()
        myView0.backgroundColor = UIColor.purple
        myView0.frame = CGRect(x:0, y:250, width:100, height:100)
//        myView0.center = view.center
        view.addSubview(myView0)
        
        let myView = UIView()
        myView.backgroundColor = UIColor.red
        myView.frame = CGRect(x:0, y:100, width:300, height:200)
        myView.alpha = 0.5
//        myView.center = view.center
//        myView.clipsToBounds = true
        view.addSubview(myView)
        
        
        let centerView = UIView()
        centerView.backgroundColor = UIColor.green
        //myView.addSubview(centerView)
        myView.addSubview(centerView)
        centerView.frame = CGRect(x: 0,
                                  y: 0,
                                  width:200,
                                  height:200 )
        centerView.center = CGPoint(x: myView.frame.width / 2,
                                    y: myView.frame.height / 2)//myView.center
        
        print(centerView.frame.size.height)
        print(centerView.frame.height)
        
        
        print(centerView.frame)
        
        print(centerView.frame.origin)
        print(centerView.frame.size)
        print(centerView.center)
        
        print(centerView.frame.minX)
        print(centerView.frame.midX)
        print(centerView.frame.maxX)
        print(centerView.frame.minY)
        print(centerView.frame.midY)
        print(centerView.frame.maxY)
        
        
        for i in stride(from: 0, to: 3, by: 1) {
            let scrollSubView = UIView()
            switch i {
            case 0: scrollSubView.backgroundColor = UIColor.red
            case 1: scrollSubView.backgroundColor = UIColor.blue
            case 2: scrollSubView.backgroundColor = UIColor.green
            default: break
            }
            
            scrollView.addSubview(scrollSubView)
            
            scrollSubView.frame = CGRect(x: CGFloat(i) * scrollView.frame.width,
                                         y:0,
                                         width: scrollView.frame.width,
                                         height: scrollView.frame.height)
//            scrollSubView.frame = CGRect(x:CGFloat(i) * scrollView.frame.width,
//                                         y:0,
//                                         width:scrollView.frame.width,
//                                         height:scrollView.frame.height)
        }
        
        scrollView.contentSize = CGSize(width: scrollView.frame.width * 3, height: scrollView.frame.height)
        
        
        textField.delegate = self
        
    }

    @IBAction func backgroundTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    
    
    
    //UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        view.backgroundColor = UIColor.black
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if(textField.text!.count > 0) {
            return true
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(Int(string) != nil) {
            return true
        }
        return false
    }
}

