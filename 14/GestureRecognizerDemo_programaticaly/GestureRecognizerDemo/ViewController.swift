//
//  ViewController.swift
//  GestureRecognizerDemo
//
//  Created by Andras Somlai
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var pinchLabel: UILabel!
    @IBOutlet weak var panLabel: UILabel!
    
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var pinchView: UILabel!
    @IBOutlet weak var rotationView: UIView!
    @IBOutlet weak var swipeView: UIView!
    @IBOutlet weak var panView: UIView!
    @IBOutlet weak var longPressView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Tagban jegyzem meg az utoljára használt fontméretet
        pinchLabel.tag = 16
        

        //UITapGestureRecognizer
        let tapGesture = UITapGestureRecognizer()
        //Ha azt akarjuk, hogy 2 érintésre aktiválódjon a gesture recognizer
        tapGesture.numberOfTapsRequired = 2
        tapGesture.addTarget(self, action: #selector(tap(gesture:)))
        tapView.addGestureRecognizer(tapGesture)
        
        //UIPinchGestureRecognizer
        let pinchGesture = UIPinchGestureRecognizer()
        pinchGesture.addTarget(self, action: #selector(pinch(_:)))
        pinchView.addGestureRecognizer(pinchGesture)
        pinchView.isUserInteractionEnabled = true
        
        //UIRotationGestureRecognizer
        let rotationGesture = UIRotationGestureRecognizer()
        rotationGesture.addTarget(self, action: #selector(rotation(_:)))
        rotationView.addGestureRecognizer(rotationGesture)
        
        //UISwipeGestureRecognizer
        let rightSwipe = UISwipeGestureRecognizer()
        rightSwipe.direction = .right
        rightSwipe.addTarget(self, action: #selector(swipe(_:)))
        swipeView.addGestureRecognizer(rightSwipe)
        
        let leftSwipe = UISwipeGestureRecognizer()
        leftSwipe.direction = .left
        leftSwipe.addTarget(self, action: #selector(swipe(_:)))
        swipeView.addGestureRecognizer(leftSwipe)
        
        let upSwipe = UISwipeGestureRecognizer()
        upSwipe.direction = .up
        upSwipe.addTarget(self, action: #selector(swipe(_:)))
        swipeView.addGestureRecognizer(upSwipe)
        
        let downSwipe = UISwipeGestureRecognizer()
        downSwipe.direction = .down
        downSwipe.addTarget(self, action: #selector(swipe(_:)))
        swipeView.addGestureRecognizer(downSwipe)
        
        //UIPanGestureRecognizer
        let pan = UIPanGestureRecognizer()
        pan.addTarget(self, action: #selector(pan(_:)))
        panView.addGestureRecognizer(pan)
        
        //UILongPressGestureRecognizer
        let longPress = UILongPressGestureRecognizer()
        longPress.minimumPressDuration = 1
        longPress.addTarget(self, action: #selector(longPress(_:)))
        longPressView.addGestureRecognizer(longPress)
        
        //UIScreenEdgePanGestureRecognizer
        let rightEdgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        rightEdgePan.edges = .right
        view.addGestureRecognizer(rightEdgePan)
        
        let leftEdgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        leftEdgePan.edges = .left
        view.addGestureRecognizer(leftEdgePan)
    }

    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        print("Screen edge swiped!")
        switch recognizer.edges {
        case .left: view.backgroundColor = UIColor.white
        case .right: view.backgroundColor = UIColor.black
        default: break
        }
    }

    @objc func tap(gesture: UITapGestureRecognizer) {
        if (gesture.view!.tag == 0) {
            gesture.view?.backgroundColor = UIColor.groupTableViewBackground
            gesture.view!.tag = 1
        }
        else {
            gesture.view?.backgroundColor = UIColor.lightGray
            gesture.view!.tag = 0
        }
    }
    
    
    @objc func pinch(_ sender: UIPinchGestureRecognizer) {
        print("pinch scale: ", sender.scale)
        let lastUsedFontSize = CGFloat(pinchLabel.tag)
        var newFontSize = lastUsedFontSize
        if(sender.scale > 1) {
            newFontSize = lastUsedFontSize+5*sender.scale
        }
        else if(sender.scale < 1) {
            newFontSize = lastUsedFontSize - 5/sender.scale
        }
        pinchLabel.font = UIFont.systemFont(ofSize: newFontSize)
        
        //Ha befejeződött a gesztus
        if (sender.state == .ended) {
            pinchLabel.tag = Int(newFontSize)
            return
        }
        
    }
    
    @objc func rotation(_ sender: UIRotationGestureRecognizer) {
        debugPrint("rotation (radian): \(sender.rotation)")
        //Azért, hogy a view legutóbbi elforgatási szögétől induljunk tovább, el kell azt tárolnom valahogy
        let lastAngle = CGFloat(sender.view!.tag)/1000
        if (sender.state == .ended) {
            sender.view!.tag = Int((lastAngle + sender.rotation)*1000)
            return
        }
//        debugPrint("rotation (radian): \(sender.rotation)  -- last saved: \(lastAngle)")
        sender.view!.transform = CGAffineTransform(rotationAngle: sender.rotation)// + lastAngle)
    }
    
    @objc func pan(_ sender: UIPanGestureRecognizer) {
        if(sender.state == .ended) {
            
            //Animáció nélkül
            panLabel.center = CGPoint(x:sender.view!.frame.width / 2,
                                      y: sender.view!.frame.height/2)
            
            //Egyszerű lineáris animációval
//            UIView.animate(withDuration: 1.0, animations: {
//                self.panLabel.center = CGPoint(x:sender.view!.frame.width / 2, y: sender.view!.frame.height/2)
//            }, completion: nil)
            
            //"Rugalmas" hatású animációval
//            UIView.animate(withDuration: 1,
//                           delay: 0,
//                           usingSpringWithDamping: 0.3,
//                           initialSpringVelocity: 0,
//                           options: .allowAnimatedContent,
//                           animations: {
//                self.panLabel.center = CGPoint(x:sender.view!.frame.width / 2, y: sender.view!.frame.height/2)
//                sender.view?.backgroundColor = UIColor.red
//            }, completion: nil)
            
            return
        }
        panLabel.center = sender.location(in: sender.view!)
    }
    
    @objc func swipe(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .left: sender.view!.backgroundColor = UIColor.red
        case .right: sender.view!.backgroundColor = UIColor.blue
        case .up: sender.view!.backgroundColor = UIColor.green
        case .down: sender.view!.backgroundColor = UIColor.lightGray
        default: sender.view!.backgroundColor = UIColor.black
        }
    }
    
    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        if(sender.state == .began) {
            sender.view?.backgroundColor = UIColor.groupTableViewBackground
        }
        else if(sender.state == .ended) {
            sender.view?.backgroundColor = UIColor.lightGray
        }
    }

}

