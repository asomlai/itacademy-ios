//
//  ViewController.swift
//  Gyakorlas
//
//  Created by Andras Somlai on 2018. 12. 06..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var data : [Bool] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        for _ in stride(from: 0, through: 100, by: 1) {
            data.append(false)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = String(indexPath.row)
        cell.selectionStyle = .none
        
        let isRowSelected = data[indexPath.row]
        
        if(isRowSelected) {
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        data[indexPath.row] = !data[indexPath.row]
        tableView.reloadData()
    }

    @IBAction func showAlertWithFavouriteNumbers(_ sender: UIButton) {
        var szamok = ""
        
        for i in stride(from: 0, to: data.count, by: 1) {
            let actual = data[i]
            if(actual == true) {
                szamok = szamok + "\(i)\n"
            }
        }
        
        let alert = UIAlertController(title: "Kedvenc számok", message: szamok, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(alertAction)
        
        present(alert, animated: true, completion: nil)
    }
}

