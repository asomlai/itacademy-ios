//
//  ViewController.swift
//  Gyakorlas2
//
//  Created by Andras Somlai on 2018. 12. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var osszeadA: UITextField!
    @IBOutlet weak var osszeadB: UITextField!
    @IBOutlet weak var kivonA: UITextField!
    @IBOutlet weak var kivonB: UITextField!
    @IBOutlet weak var szorozA: UITextField!
    @IBOutlet weak var szorozB: UITextField!
    @IBOutlet weak var osztA: UITextField!
    @IBOutlet weak var osztB: UITextField!
    @IBOutlet weak var modA: UITextField!
    @IBOutlet weak var modB: UITextField!
    
    @IBOutlet weak var osszadE: UILabel!
    @IBOutlet weak var kivonE: UILabel!
    @IBOutlet weak var szorozE: UILabel!
    @IBOutlet weak var osztE: UILabel!
    @IBOutlet weak var modE: UILabel!
    
    @IBOutlet weak var korabbiEredmenyek: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        korabbiEredmenyek.text = ""
    }

    @IBAction func osszead(_ sender: Any) {
        let a = Int(osszeadA.text!)
        let b = Int(osszeadB.text!)
        
        if(a != nil && b != nil) {
            let eredmeny = a! + b!
            osszadE.text = String(eredmeny)
            
            korabbiEredmenyek.text = "\n \(a!) + \(b!) = \(eredmeny)" + korabbiEredmenyek.text
        }
        else {
            osszadE.text = "???"
        }
    }
    
    @IBAction func kivon(_ sender: Any) {
        let a = Int(kivonA.text!)
        let b = Int(kivonB.text!)
        
        if(a != nil && b != nil) {
            kivonE.text = String(a! - b!)
            korabbiEredmenyek.text = "\n \(a!) - \(b!) = \(a! - b!)" + korabbiEredmenyek.text
        }
        else {
            kivonE.text = "???"
        }
    }
    
    @IBAction func szoroz(_ sender: Any) {
        let a = Int(szorozA.text!)
        let b = Int(szorozB.text!)
        
        if(a != nil && b != nil) {
            szorozE.text = String(a! * b!)
            korabbiEredmenyek.text = "\n \(a!) * \(b!) = \(a! * b!)" + korabbiEredmenyek.text
        }
        else {
            szorozE.text = "???"
        }
    }
    
    @IBAction func oszt(_ sender: Any) {
        let a = Double(osztA.text!)
        let b = Double(osztB.text!)
        
        if(a != nil && b != nil && b != 0) {
            osztE.text = String(a! / b!)
            korabbiEredmenyek.text = "\n \(a!) / \(b!) = \(a! / b!)" + korabbiEredmenyek.text
        }
        else {
            osztE.text = "???"
        }
    }
    
    @IBAction func modulo(_ sender: Any) {
        let a = Int(modA.text!)
        let b = Int(modB.text!)
        
        if(a != nil && b != nil) {
            modE.text = String(a! % b!)
            korabbiEredmenyek.text = "\n \(a!) % \(b!) = \(a! % b!)" + korabbiEredmenyek.text
        }
        else {
            modE.text = "???"
        }
    }
    
    @IBAction func torles(_ sender: Any) {
        korabbiEredmenyek.text = ""
    }
    
}

