//
//  ViewController.swift
//  Gyakorlas3
//
//  Created by Andras Somlai on 2018. 12. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //a/i
        var integer = 5
        
        //a/ii
        var floatNumber : Float = 3.14
        
        //a/iii
        if(Float(integer) > floatNumber) {
            print("integer nagyobb")
        }
        else {
            print("float nagyobb, vagy egyenlő a két szám")
        }
        
        //a/iv
        var tomb : [Bool] = [false, false, true, false, false]
        
        //a/v
        let dictionary : [Int : String] = [1 : "egy", 2 : "kettő", 3 : "három"]
        
        //b/iii
        containsTrue(array: tomb)
        print(doesArrayContainsTrue(array: tomb))
        
        
        //d/i
        print(Calculator.osszead(a: 1, b: 3))
        print(Calculator.osszead(a: 3.5, b: 6))
        print(Calculator.osszead(a: 5435, b: -5435))
        
        //d/ii
        print(Calculator.szoroz(a: 1, b: 3))
        print(Calculator.szoroz(a: 3.5, b: 6))
        print(Calculator.szoroz(a: 0, b: -5435))
        
        //d/iii
        print(Calculator.oszt(a: 1, b: 3))
        print(Calculator.oszt(a: 3.5, b: 6))
        print(Calculator.oszt(a: 5435, b: 0))
        
        
        //f/i
        let newPerson = Person(newName: "Geza", newAddress: "Andrássy út 1", newGender: .male, newBirthyear: 1980)
        
        //f/ii
        print(newPerson.age)
    }

    //b/i
    func containsTrue(array: [Bool]) {
        for item in array {
            if(item == true) {
                print("A tömbben van true érték")
                return
            }
        }
    }
    
    //b/ii
    func doesArrayContainsTrue(array: [Bool]) -> Bool {
        for item in array {
            if(item == true) {
                return true
            }
        }
        
        return false
    }
    
}

