//
//  Calculator.swift
//  Gyakorlas3
//
//  Created by Andras Somlai on 2018. 12. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Calculator: NSObject {

    //c/i
    static func osszead(a: Double, b: Double) -> Double {
        return a+b
    }
    
    //c/ii
    static func szoroz(a: Double, b: Double) -> Double {
        return a*b
    }
    
    //c/iii
    static func oszt(a: Double, b: Double) -> Double {
        if(b == 0) {
            return 0
        }
        return a/b
    }
}
