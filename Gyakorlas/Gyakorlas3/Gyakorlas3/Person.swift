//
//  Person.swift
//  Gyakorlas3
//
//  Created by Andras Somlai on 2018. 12. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum Gender {
    case male
    case female
}

class Person: NSObject {

    //e/i
    var name: String?
    var address: String?
    var gender: Gender?
    var birthyear : Int?
    
    //e/ii
    var age : Int? {
        if(birthyear == nil) {
            return nil
        }
        return 2018 - birthyear!
    }
    
    //e/iii
    init(newName: String, newAddress: String, newGender: Gender, newBirthyear: Int) {
        name = newName
        address = newAddress
        gender = newGender
        birthyear = newBirthyear
    }
}
