//
//  ViewController.swift
//  RatePanelFromXib
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ratePanelContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Xib-ből így lehet custom view-t betölteni
        if let ratePanel = Bundle.main.loadNibNamed("RatePanel", owner: nil, options: nil)![0] as? RatePanel {
            //A storyboardban létrehozott containerviewhoz hozzáadjuk
            ratePanelContainer.addSubview(ratePanel)
        }
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //Ha hozzá van adva az értékelőpanel a ratePanelContainer-hoz
        if(ratePanelContainer.subviews.count > 0) {
            //Beállítom, hogy pontosan ugyanakkora legyen, mint a container
            ratePanelContainer.subviews[0].frame = ratePanelContainer.bounds
        }
    }

}

