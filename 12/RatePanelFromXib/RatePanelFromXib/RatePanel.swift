//
//  RatePanel.swift
//  RatePanelFromXib
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class RatePanel: UIView {

    @IBAction func rated(_ sender: UIButton) {
        switch sender.tag {
        case 1: debugPrint("LIKE")
        case 2: debugPrint("NEUTRAL")
        case 3: debugPrint("DISLIKE")
        default: debugPrint("UNKNOWN TAG")
        }
        
        //Egy view-t így távolíthatsz el a superviewról
        self.removeFromSuperview()
    }
}
