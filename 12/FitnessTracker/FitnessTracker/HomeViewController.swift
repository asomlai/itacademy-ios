//
//  ViewController.swift
//  FitnessTracker
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var bodyHeight: UITextField!
    @IBOutlet weak var bodyWeight: UITextField!
    @IBOutlet weak var chest: UITextField!
    @IBOutlet weak var stomach: UITextField!
    @IBOutlet weak var rightThigh: UITextField!
    @IBOutlet weak var leftThigh: UITextField!
    @IBOutlet weak var rightBiceps: UITextField!
    @IBOutlet weak var leftBiceps: UITextField!
    
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    var bodyData : BodyData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Ha van mentett adat, a bodyData-t abból töltöm fel adattal
        if (UserDefaults.standard.dictionary(forKey: "savedData") != nil) {
            bodyData = BodyData(dictionary: UserDefaults.standard.dictionary(forKey: "savedData")!)
        }
        //Ha nincs, létrehozok egy üres objektumot
        else {
            bodyData = BodyData()
        }
        
        //Billentyűzet elrejtéséhez background tap hozzáadása
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUIData()
    }

    func updateUIData() {
        //Inline if-ekkel frissítem a felhasználói felületet. Azért hazsnálok inline if-et, hogy rövidebb legyen a kód
        //Az inline if szintaktikája:
        //(Ha ez a feltétel teljesül) ? (történjen ez) : (egyébként történjen ez)
        //Az inline ifek esetében nincs else if-ág, csak if van, és else
        //Normál if szintaktika:
        //if(bodyData.bodyHeight != nil) {
        //    bodyHeight.text = String(bodyData.bodyHeight!)
        //}
        //else {
        //    bodyHeight.text = ""
        //}
        //UGYANEZ INLINE IFFEL:
        //(bodyData.bodyHeight != nil) ? (bodyHeight.text = String(bodyData.bodyHeight!)) : (bodyHeight.text = "")
        
        
        (bodyData.bodyHeight != nil) ? (bodyHeight.text = String(bodyData.bodyHeight!)) : (bodyHeight.text = "")
        (bodyData.bodyWeight != nil) ? (bodyWeight.text = String(bodyData.bodyWeight!)) : (bodyWeight.text = "")
        (bodyData.chest != nil) ? (chest.text = String(bodyData.chest!)) : (chest.text = "")
        (bodyData.stomach != nil) ? (stomach.text = String(bodyData.stomach!)) : (stomach.text = "")
        (bodyData.rightThigh != nil) ? (rightThigh.text = String(bodyData.rightThigh!)) : (rightThigh.text = "")
        (bodyData.leftThigh != nil) ? (leftThigh.text = String(bodyData.leftThigh!)) : (leftThigh.text = "")
        (bodyData.rightBiceps != nil) ? (rightBiceps.text = String(bodyData.rightBiceps!)) : (rightBiceps.text = "")
        (bodyData.leftBiceps != nil) ? (leftBiceps.text = String(bodyData.leftBiceps!)) : (leftBiceps.text = "")
        
        (bodyData.gender != nil) ? (genderLabel.text = bodyData.gender!.rawValue) : (genderLabel.text = "UNKNOWN")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy, MMM d, HH:mm:ss"
        (bodyData.lastUpdated != nil) ? (lastUpdatedLabel.text = dateFormatter.string(from: bodyData.lastUpdated!)) : (lastUpdatedLabel.text = "-")
    }
    
    @IBAction func saveData(_ sender: UIButton) {
        //A bodyData propertyjeit módosítom a felületen bevitt értékek szerint
        bodyData.bodyHeight = Double(bodyHeight.text ?? "")
        bodyData.bodyWeight = Double(bodyWeight.text ?? "")
        bodyData.chest = Double(chest.text ?? "")
        bodyData.stomach = Double(stomach.text ?? "")
        bodyData.rightThigh = Double(rightThigh.text ?? "")
        bodyData.leftThigh = Double(leftThigh.text ?? "")
        bodyData.rightBiceps = Double(rightBiceps.text ?? "")
        bodyData.leftBiceps = Double(leftBiceps.text ?? "")
        bodyData.lastUpdated = Date()
        
        //Elmentem defaultsba a bodyData-t
        UserDefaults.standard.set(bodyData.getAsDictionary(), forKey: "savedData")
        
        //Frissítem a felhasználói felületet
        updateUIData()
    }
    
    //Billzet elrejtése
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    //Navigáció
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "openSettings") {
            (segue.destination as! SettingsViewController).bodyData = self.bodyData
        }
    }
}

