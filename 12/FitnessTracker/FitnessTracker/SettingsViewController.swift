//
//  SettingsViewController.swift
//  FitnessTracker
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    @IBOutlet weak var genderSelector: UISegmentedControl!
    //BodyData-t a másik viewController mindig beállítja mielőtt betöltődne ez, úgyhogy mehet a ! a típus mögé
    var bodyData : BodyData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Amikor betöltődik a képernyő, jelenítse is meg helyesen az adatokat
        updateUI()
    }

    func updateUI() {
        //Ha meg van adva a felhasználó neme
        if (bodyData.gender != nil) {
            //Inline iffel megadva.
            //genderSelector.selectedSegmentIndex = HA (bodyData.gender == .female) AKKOR 0 EGYÉBKÉNT 1
            genderSelector.selectedSegmentIndex = bodyData.gender == .female ? 0 : 1
            
            //Ugyanaz a logika switch case-zel
            //switch bodyData.gender! {
            //case .female: genderSelector.selectedSegmentIndex = 0
            //case .male: genderSelector.selectedSegmentIndex = 1
            //}
            
            //Ugyanaz if-else-szel
            //if(bodyData.gender == .female) {
            //    genderSelector.selectedSegmentIndex = 0
            //}
            //else {
            //    genderSelector.selectedSegmentIndex = 1
            //}
        }
        //Ha nincs megadva a felhasználó neme
        else {
            //Ne legyen semelyik szegmens kijelölve
            genderSelector.selectedSegmentIndex = -1
        }
    }

    //Ha a felhasználó kiválasztja a nemét (IBACTION)
    @IBAction func genderSelected(_ sender: UISegmentedControl) {
        bodyData.gender = sender.selectedSegmentIndex == 0 ? .female : .male
    }
    
    //Ha a felhasználó törli az appba bevitt adatokat
    @IBAction func clearAppData(_ sender: UIButton) {
        //Töröljük defaultsból
        UserDefaults.standard.removeObject(forKey: "savedData")
        //Töröljük a memóriából
        bodyData.clear()
        //Frissítjük a felhasználói felületet
        updateUI()
    }
    
    //Ha értékelni szeretné a felhasználó az appot
    @IBAction func rateApp(_ sender: UIButton) {
        //ratePanel custom view betöltése
        let ratePanel = Bundle.main.loadNibNamed("RatePanel", owner: nil, options: nil)![0] as! RatePanel
        
        //ratePanel méretének megadása
        ratePanel.frame = CGRect(x:0, y:0, width:300, height:250)
        
        //ratePanel pozícionálása középre
        ratePanel.center = view.center
        
        //view-hoz hozzádás
        view.addSubview(ratePanel)
    }
    
}
