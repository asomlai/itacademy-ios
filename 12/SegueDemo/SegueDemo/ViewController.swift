//
//  ViewController.swift
//  SegueDemo
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var numberDataLabel: UILabel!
    
    var data = Data()
    var data2 = Data()
    var data3 = Data()
    var data4 = Data()
    var data5 = Data()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Tap gesture recognizer: érintésre reagál
        let tap = UITapGestureRecognizer()
        //Itt adom meg, hogy mi történjen, ha aktiválódik
        //első param: target: self --> tehát ebben az osztályban fogja ekeresni a függvényt ami lefut, ha működésbe lép
        //második param: action: selector(hideKeyboard) --> a hideKeyboard 0 paraméteres fgv fog lefutni a tap hatására
        tap.addTarget(self, action: #selector(hideKeyboard))
        //A viewcontroller view-jához adom hozzá a gesture recognizert, így ha a háttérre tappolunk, akkor fog aktiválódni
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.data.numberData != nil) {
            self.numberDataLabel.text = String(self.data.numberData!)
        }
        else {
            self.numberDataLabel.text = "Nincs numberData"
        }
        
    }

    @objc func hideKeyboard() {
        //View-n, és a hozzá tartozó összes subview-n a szerkesztés befejezése
        //Tehát eltünteti a billentyűzetet
        view.endEditing(true)
    }
    
     // MARK: - Navigation
     
     // Ha az alkalmazás navigációja storyboard alapú, ezzel a függvénnyel tudod a navigáció előtti beállításokat elvégezni
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("MainVC - prepare for segue")
     // A cél viewControllert a segue.destinationViewController-rel éred el
     // A segue.identitfier a storyboardban megadott azonosítója a seguenek (nyílnak)
        if(segue.identifier == "showSecondViewController") {
            //A showSecondViewController azonosítójú segue destinationje SecondViewController típusú kell legyen
            if let secondVC = segue.destination as? SecondViewController {
                //CRASH, mert ekkor még nem töltöt be a UI
                //secondVC.label.text = textField.text
                
                //Elválasztom a modelt a view-tól, és nem UI-t müdosítok!
                secondVC.textToShow = textField.text
            }
        }
        else if(segue.identifier == "blueSegueId") {
            if let thirdVC = segue.destination as? ThirdViewController {
                //Elválasztom a modelt a view-tól, és nem UI-t müdosítok!
                self.data.stringData = textField.text
                thirdVC.data = self.data
            }
        }
     }
 
}

