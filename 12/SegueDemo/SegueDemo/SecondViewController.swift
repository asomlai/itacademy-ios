//
//  SecondViewController.swift
//  SegueDemo
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    var textToShow : String?
    @IBOutlet weak var label : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("SecondVC - viewDidLoad")
//        Hosszú megoldás
//        if(textToShow != nil) {
//            label.text = textToShow
//        }
//        else {
//            label.text = "nincs adat"
//        }
        
//        1 soros megoldás
        label.text = textToShow ?? "nincs adat"
    }


}
