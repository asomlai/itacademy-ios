//
//  ThirdViewController.swift
//  SegueDemo
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    var data : Data?
    @IBOutlet weak var dataLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Ha data property be van állítva
        if(data?.stringData != nil) {
            //kiírom a stringData propertyjét
            dataLabel.text = data?.stringData
        }
        
        //A parent viewcontroller már be van töltve, móüdosítom a felhasználói felületét
        if let parentVc = navigationController?.viewControllers[0] as? ViewController {
            parentVc.textField.text = "Blue"
        }
        
        //Ha a data propertynek van értéke, módosítom
        if(data != nil) {
            data!.numberData = 12
        }
    }

}
