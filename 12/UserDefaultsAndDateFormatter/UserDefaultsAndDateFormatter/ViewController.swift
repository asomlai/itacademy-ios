//
//  ViewController.swift
//  UserDefaultsAndDateFormatter
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dateformatTextField: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    
    /*
     Example date formats
     EEEE, MMM d, yyyy ------ Tuesday, Apr 24, 2018
     MM/dd/yyyy ------ 04/24/2018
     MM-dd-yyyy HH:mm ------ 04-24-2018 12:12
     MMM d, h:mm a ------ Apr 24, 12:12 PM
     MMMM yyyy ------ April 2018
     MMM d, yyyy ------ Apr 24, 2018
     E, d MMM yyyy HH:mm:ss Z ------ Tue, 24 Apr 2018 12:12:01 +0000
     yyyy-MM-dd'T'HH:mm:ssZ ------ 2018-04-24T12:12:01+0000
     dd.MM.yy ------ 24.04.18
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        UserDefaults.standard.removeObject(forKey: "szam")
    }

    @IBAction func saveToDefaults(_ sender: UIButton) {
        //Ha a dateformatTextField.text értéke nem nil, és nem is üres string
        if(dateformatTextField.text != nil && dateformatTextField.text!.isEmpty == false) {
            //Elmentem defaultsba az értékét "SavedDateFormat" kulccsal
            UserDefaults.standard.set(dateformatTextField.text!, forKey: "SavedDateFormat")
        }
        
        UserDefaults.standard.set(2, forKey: "szam")
        UserDefaults.standard.set(true, forKey: "bool")
        UserDefaults.standard.set([1,2,3,5,6,7], forKey: "tomb")
        UserDefaults.standard.set(["egy" : 1, "ketto" : 2], forKey: "dict")
    }
    
    @IBAction func loadFromDefaults(_ sender: UIButton) {
        //Ha be tudok olvasni string típusú értéket defaultsból "SavedDateFormat" kulccsal
//        if let loadedDateFormat = UserDefaults.standard.string(forKey: "SavedDateFormat") {
//            //A textfield szövegét beállítom annak megfelelően
//            dateformatTextField.text = loadedDateFormat
//        }
        
        let loadedDateFormat = UserDefaults.standard.string(forKey: "SavedDateFormat")
        dateformatTextField.text = loadedDateFormat
        
        print(UserDefaults.standard.object(forKey: "szam"))
        print(UserDefaults.standard.object(forKey: "bool") as! Bool)
        
        print(UserDefaults.standard.integer(forKey: "szam"))
        print(UserDefaults.standard.bool(forKey: "bool"))
        print(UserDefaults.standard.array(forKey: "tomb") as! [Int])
        print(UserDefaults.standard.dictionary(forKey: "dict") as! [String : Int])
    }
    
    
    @IBAction func updateDateLabel(_ sender: UIButton) {
        //Létrehozok egy dateFormattert és beállítom a dateFormatot rajta
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateformatTextField.text
        
        //Adott időpillanat lekérdezése Date formátumban
        let now = Date()
        
        //A dateformatterrel a megadott format szerint megpróbálom stringgé alakítani a dátumot
        let dateString = dateFormatter.string(from: now)
        
        //Ha a dateString üres, akkor hibás a dateformat amit a user beírt a szövegmezőbe
        if(dateString.isEmpty == true) {
            dateLabel.text = "\(dateformatTextField.text!) nem értelmezhető"
        }
        //Egyébként kiírjuk a dátumot
        else {
            dateLabel.text = dateString
        }

    }
}

