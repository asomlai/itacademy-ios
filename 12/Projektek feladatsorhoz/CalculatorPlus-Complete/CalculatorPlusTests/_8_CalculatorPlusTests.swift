//
//  _8_CalculatorPlusTests.swift
//  08-CalculatorPlusTests
//
//  Created by Andras Somlai on 2018. 04. 12..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import XCTest

class _8_CalculatorPlusTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCalculator() {
        let calculator = Calculator()
        let operation1 = Operation(operands: (operand1: 1.34, operand2: 1), operationType: .multiplication, calculator: calculator)
        let result1 = calculator.calculateOperationResult(operation: operation1, shouldSaveResult: true)
        XCTAssertEqual(1.34, result1)
        XCTAssertEqual(1, calculator.history.count)
        
        let result2 = calculator.calculateOperationResult(operation: operation1, shouldSaveResult: false)
        XCTAssertEqual(1.34, result2)
        XCTAssertEqual(1, calculator.history.count)
        
        let operation3 = Operation(operands: (operand1: 134.23, operand2: 10), operationType: .modulo, calculator: calculator)
        let result3 = calculator.calculateOperationResult(operation: operation3, shouldSaveResult: true)
        XCTAssertEqual(4, result3)
        XCTAssertEqual(2, calculator.history.count)
        
        let operation4 = Operation(operands: (operand1: 134.53, operand2: 10), operationType: .modulo, calculator: calculator)
        let result4 = calculator.calculateOperationResult(operation: operation4, shouldSaveResult: false)
        XCTAssertEqual(5, result4)
        XCTAssertEqual(2, calculator.history.count)
        
    }
    
    func testGetOperationType() {
        XCTAssertEqual(OperationType.addition, Calculator.getOperationType(from: "3+2"))
        XCTAssertEqual(OperationType.substraction, Calculator.getOperationType(from: "3-2"))
        XCTAssertEqual(OperationType.multiplication, Calculator.getOperationType(from: "3*2"))
        XCTAssertEqual(OperationType.division, Calculator.getOperationType(from: "3/2"))
        XCTAssertEqual(OperationType.modulo, Calculator.getOperationType(from: "3%2"))
        
        XCTAssertEqual(OperationType.addition, Calculator.getOperationType(from: "3 +   2"))
        XCTAssertEqual(OperationType.unknown, Calculator.getOperationType(from: "3 +-   2"))
        XCTAssertEqual(OperationType.addition, Calculator.getOperationType(from: "3,14 +   2.43"))
        XCTAssertEqual(OperationType.modulo, Calculator.getOperationType(from: "3,14 %   2.43"))
        XCTAssertEqual(OperationType.unknown, Calculator.getOperationType(from: "3,14 %   2.43 % 34"))
        XCTAssertEqual(OperationType.unknown, Calculator.getOperationType(from: "3+2+4"))
    }
    
    func testGetOperands() {
        XCTAssertEqual(3.0, Calculator.getOperands(from: "3+2", operationType: .addition)!.0)
        XCTAssertEqual(2.0, Calculator.getOperands(from: "3+2", operationType: .addition)!.1)
        
        XCTAssertEqual(3.4, Calculator.getOperands(from: "3.4 + 2,32", operationType: .addition)!.0)
        XCTAssertEqual(2.32, Calculator.getOperands(from: "3.4 % 2,32", operationType: .modulo)!.1)
        XCTAssertEqual(2.32, Calculator.getOperands(from: "3.4/2,32", operationType: .division)!.1)
        XCTAssertEqual(2.32, Calculator.getOperands(from: "3.4*2,32", operationType: .multiplication)!.1)
        XCTAssertEqual(2.32, Calculator.getOperands(from: "3.4-2,32", operationType: .substraction)!.1)
        
        XCTAssertNil(Calculator.getOperands(from: "3.4 + 2,32.32", operationType: .addition))
        XCTAssertNil(Calculator.getOperands(from: "3.4 != 2,32", operationType: .modulo))
        XCTAssertNil(Calculator.getOperands(from: "3+2", operationType: .modulo))
    }
    
    func testCasting() {
        XCTAssertEqual(3, Calculator.castToIntCorrectly(3.49999))
        XCTAssertEqual(4, Calculator.castToIntCorrectly(3.5))
        XCTAssertEqual(-4, Calculator.castToIntCorrectly(-3.5))
        XCTAssertEqual(0, Calculator.castToIntCorrectly(0))
        
        XCTAssertEqual(4.2, Calculator.castToDoubleIfPossible(doubleStringNumber: "4.2"))
        XCTAssertEqual(4.2, Calculator.castToDoubleIfPossible(doubleStringNumber: "4,2"))
        XCTAssertEqual(4.2352, Calculator.castToDoubleIfPossible(doubleStringNumber: "4,2352"))
        XCTAssertEqual(4, Calculator.castToDoubleIfPossible(doubleStringNumber: "4"))
        XCTAssertEqual(nil, Calculator.castToDoubleIfPossible(doubleStringNumber: "4 "))
        XCTAssertEqual(nil, Calculator.castToDoubleIfPossible(doubleStringNumber: "4 2"))
        XCTAssertEqual(nil, Calculator.castToDoubleIfPossible(doubleStringNumber: "4 .2"))
    }
    
}
