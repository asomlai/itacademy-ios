//
//  _8_ProductStockTests.swift
//  08-ProductStockTests
//
//  Created by Andras Somlai on 2018. 04. 16..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import XCTest

class _8_ProductStockTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCode() {
        XCTAssertEqual(300, CurrencyCalculator.exchange(sourceCurrency: .eur, destinationCurrency: .huf, money: 1))
        XCTAssertEqual(250, Int(CurrencyCalculator.exchange(sourceCurrency: .usd, destinationCurrency: .huf, money: 1)+0.5))
        XCTAssertEqual(1, CurrencyCalculator.exchange(sourceCurrency: .huf, destinationCurrency: .huf, money: 1))
        XCTAssertEqual(1, CurrencyCalculator.exchange(sourceCurrency: .eur, destinationCurrency: .eur, money: 1))
        XCTAssertEqual(1, CurrencyCalculator.exchange(sourceCurrency: .usd, destinationCurrency: .usd, money: 1))
        XCTAssertEqual(2500, Int(CurrencyCalculator.exchange(sourceCurrency: .usd, destinationCurrency: .huf, money: 10)+0.5))
        XCTAssertEqual(3300, Int(CurrencyCalculator.exchange(sourceCurrency: .eur, destinationCurrency: .huf, money: 11)+0.5))
        XCTAssertEqual(-2500, Int(CurrencyCalculator.exchange(sourceCurrency: .usd, destinationCurrency: .huf, money: -10)-0.5))
        XCTAssertEqual(-3300, Int(CurrencyCalculator.exchange(sourceCurrency: .eur, destinationCurrency: .huf, money: -11)-0.5))
    }
    
}
