//
//  ViewController.swift
//  Storyboard
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var rateViewContainer: UIView!
    //let rateView = Bundle.main.loadNibNamed("CustomView2", owner: nil, options: nil)![0] as! CustomView2//CustomView.fromNib()
    
    let rateView = CustomView.fromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rateViewContainer.addSubview(rateView)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        rateView.frame = rateViewContainer.bounds
        rateView.backgroundColor = UIColor.purple
    }

}

