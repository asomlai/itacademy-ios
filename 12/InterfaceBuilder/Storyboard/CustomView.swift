//
//  CustomView.swift
//  Storyboard
//
//  Created by Andras Somlai.
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class CustomView: UIView {

    @IBAction func buttonTocuhed(_ sender: UIButton) {
        if(sender.tag == 1) {
            debugPrint("LIKE")
        }
        else if(sender.tag == 2) {
            debugPrint("DISLIKE")
        }
        else {
            debugPrint("UNKNOWN")
        }
    }
    
    static func fromNib() -> CustomView {
        return Bundle.main.loadNibNamed("CustomView", owner: nil, options: nil)![0] as! CustomView
    }
}
