//
//  ViewController.swift
//  RegistrationForm
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let profile = Profile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsVC = segue.destination as? DetailsViewController {
            detailsVC.profile = profile
        }
        
        if let viewProfileVC = segue.destination as? ViewProfileViewController {
            viewProfileVC.profile = profile
        }
    }
    
}

