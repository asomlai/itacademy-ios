//
//  ViewController.swift
//  MemoryDemo
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class NotebookStoreViewController: UIViewController {

    var myPerson : Person? = Person()
    var myNotebook : Notebook? = Notebook()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        myPerson?.age = 18
//        myPerson?.printData()
//
//        print("myNotebook:", myNotebook)
//        print("person's notebook:", myPerson?.notebook)
        
        
        var specPerson = Person()
        specPerson.name = "Special Guy"
        
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        myPerson?.notebook = myNotebook
        
        self.myNotebook = nil
        
        
        //self.myPerson = nil
    }


}

