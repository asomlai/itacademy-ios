//
//  Notebook.swift
//  MemoryDemo
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Notebook {
    
    var producer : String = "Apple"
    var model : String = "Macbook Pro"
    var color : UIColor = UIColor.black
    weak var owner : Person?
    
    deinit {
        print("notebook with model: \(model) deinit called")
    }
    
    func printData() {
        print("""
            producer: \(producer)
            model: \(model)
        """)
    }
}
