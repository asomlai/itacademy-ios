//
//  Person.swift
//  MemoryDemo
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum Gender : String {
    case male
    case female
}

class Person {
    var name : String = "John Doe"
    var age : Int = 30
    var address : String = "USA"
    var gender : Gender = .male
    var notebook : Notebook? {
        didSet {
            notebook?.owner = self
        }
    }
    
    deinit {
        print("person with name: \(name) deinit called")
    }
    
    func printData() {
        print("""
            name: \(name)
            age: \(age)
            address: \(address)
            gender: \(gender.rawValue)
            notebook: \(notebook)
        """)
    }
    
}
