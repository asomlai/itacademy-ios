//
//  ViewController.swift
//  MemoryDemo
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var myPerson : Person? = Person()
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        myPerson?.age = 18
        myPerson?.printData()
        
        myPerson = Person()
        myPerson?.printData()

    }


}

