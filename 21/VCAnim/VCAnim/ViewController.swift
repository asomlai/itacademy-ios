//
//  ViewController.swift
//  VCAnim
//
//  Created by Andras Somlai on 2018. 12. 06..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func btn(_ sender: Any) {
        let greenView = UIView()
        greenView.frame = CGRect(x:view.frame.width/2,
                                 y:view.frame.height/2,
                                 width: 0,
                                 height:0)
        greenView.backgroundColor = UIColor.green
//        greenView.alpha = 0
        view.addSubview(greenView)
        
        UIView.animate(withDuration: 0.5, animations: {
            greenView.frame = self.view.bounds
        }) { (_) in
            self.performSegue(withIdentifier: "show", sender: nil)
        }
        
    }
    
}

