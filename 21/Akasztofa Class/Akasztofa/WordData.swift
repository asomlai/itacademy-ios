//
//  WordData.swift
//  Akasztofa
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class WordData {

    var szo : String
    var isUsed : Bool = false
    
    init(_ word: String) {
        szo = word
    }
    
    func printData() {
        print("\(szo) - \(isUsed)")
    }
}
