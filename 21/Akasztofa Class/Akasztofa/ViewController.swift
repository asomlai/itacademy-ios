//
//  ViewController.swift
//  Akasztofa
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var szavak : [WordData] = [WordData("Alma"),
                               WordData("körte"),
                               WordData("narancs"),
                               WordData("macska"),
                               WordData("telefon"),
                               WordData("auto")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(getWord())
        print(getWord())
        print(getWord())
        print(getWord())
        print(getWord())
        print(getWord())

        

    }
    
    
    func getWord() -> String {
        var random = Int.random(in: 0...szavak.count-1)
        
        var generaltElem = szavak[random]
        
        
        while (generaltElem.isUsed == true) {
            random = Int.random(in: 0...szavak.count-1)
            generaltElem = szavak[random]
        }
        
        generaltElem.isUsed = true
        
        szavak.map{$0.printData()}
        
        return generaltElem.szo
    }

}

