//
//  Notebook.swift
//  ClassAndStruct
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

struct Notebook {
    var producer : String = "Apple"
    var model : String = "Macbook Pro"
    var color : UIColor = UIColor.black

    func printData() {
        print("""
            producer: \(producer)
            model: \(model)
            """)
    }
}
