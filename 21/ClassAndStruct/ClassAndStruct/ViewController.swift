//
//  ViewController.swift
//  ClassAndStruct
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var myPerson: Person = Person()
    var myNotebook: Notebook = Notebook()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myNotebook.model = "Macbook Air"
        
        myPerson.notebook = self.myNotebook
        myPerson.notebook?.model = "Mac Mini"
        
        myNotebook.printData()
        myPerson.notebook?.printData()
        
    }
}

