//
//  ViewController.swift
//  Akasztofa
//
//  Created by Andras Somlai on 2018. 11. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var szavak : [[String : Any]] = [["szo" : "Alma", "isUsed" : false],
                                     ["szo" : "körte", "isUsed" : false],
                                     ["szo" : "narancs", "isUsed" : false],
                                     ["szo" : "macska", "isUsed" : false],
                                     ["szo" : "telefon", "isUsed" : false],
                                     ["szo" : "auto", "isUsed" : false]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(getWordCorrect())
        print(getWordCorrect())
        print(getWordCorrect())
        print(getWordCorrect())
        print(getWordCorrect())
        print(getWordCorrect())
        

    }
    
    
    func getWordWrong() -> String {
        var random = Int.random(in: 0...szavak.count-1)
        
        var generaltElem = szavak[random]
        
        
        while (generaltElem["isUsed"] as! Bool == true) {
            random = Int.random(in: 0...szavak.count-1)
            generaltElem = szavak[random]
        }
        
        generaltElem["isUsed"] = true
        
        print(szavak)
        
        return generaltElem["szo"] as! String
    }

    
    func getWordCorrect() -> String {
        var random = Int.random(in: 0...szavak.count-1)
        
        while (szavak[random]["isUsed"] as! Bool == true) {
            random = Int.random(in: 0...szavak.count-1)
        }
        
        szavak[random]["isUsed"] = true
        
        print(szavak)
        
        return szavak[random]["szo"] as! String
    }


}

