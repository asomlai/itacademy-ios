//
//  ViewController.swift
//  CoreMotionExample
//
//  Created by Maxim Bilan on 1/21/16.
//  Copyright © 2016 Maxim Bilan. All rights reserved.
//

import UIKit
import CoreMotion

import CoreLocation //for altitude

class ViewController: UIViewController {
    
    private let motionManager = CMMotionManager()
    
    private let activityManager = CMMotionActivityManager()
    private let pedometer = CMPedometer()
    
    private let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
        motionManager.startAccelerometerUpdates()
        motionManager.startGyroUpdates()
        motionManager.startMagnetometerUpdates()
        motionManager.startDeviceMotionUpdates()
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(ViewController.update), userInfo: nil, repeats: true)
         */
//
        startTrackingActivityType()
//        startCountingSteps()
        
        /*
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
         */
    }
    
    @objc func update() {
        /*
        if let accelerometerData = motionManager.accelerometerData {
            print(accelerometerData)
        }
 
        
        if let gyroData = motionManager.gyroData {
            print(gyroData)
        }
        */
        
        /*
        if let magnetometerData = motionManager.magnetometerData {
            print(magnetometerData)
        }
         */
        
        /*
        if let deviceMotion = motionManager.deviceMotion {
            print(deviceMotion)
        }
         */
    }
    
    
    private func startTrackingActivityType() {
        if #available(iOS 11.0, *) {
            debugPrint(CMMotionActivityManager.authorizationStatus() == .authorized)
        } else {
            // Fallback on earlier versions
        }
        
        activityManager.startActivityUpdates(to: OperationQueue.current!) { (data) in
            guard let activity = data else { return }
            DispatchQueue.main.async {
                if activity.walking {
                    print("Walking")
                } else if activity.stationary {
                    print("Stationary")
                } else if activity.running {
                    print("Running")
                } else if activity.automotive {
                    print("Automotive")
                }
            }
        }
    }
    
    private func startCountingSteps() {
        if #available(iOS 11.0, *) {
            debugPrint(CMPedometer.authorizationStatus() == .authorized)
        } else {
            // Fallback on earlier versions
        }
        
        pedometer.startUpdates(from: Date()) { (data, error) in
            guard let pedometerData = data, error == nil else { return }
            debugPrint(pedometerData.numberOfSteps.stringValue)
        }
    }
}

extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let alt = locations[0].altitude
        print(alt)
    }
}
