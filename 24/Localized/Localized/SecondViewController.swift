//
//  SecondViewController.swift
//  Localized
//
//  Created by Andras Somlai on 2018. 06. 05..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        label.text = "text"
        label.text = NSLocalizedString("text", comment: "textComment")
//        label.text = "text".localized(lang: "hu")
        
        
        let language = UserDefaults.standard.string(forKey: "MyAppLanguage")!
        label.text = "text".localized(lang: language)
    }

}
