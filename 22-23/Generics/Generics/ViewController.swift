//
//  ViewController.swift
//  Generics
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var valtozo: Int = 0 {
        didSet {
            print(oldValue)
        }
        
        willSet {
            print(newValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let number : Double = 1
        
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
        
        var res : Double = 0
        print("try0")
        do {
            
            res = try Calculator.divide(a: 10, b: 0)
            
            //...
        }
        catch {
            print("catch: \(error)")
        }
        
        print(res)
        print("----END----")
        
        
        do {
            let result = try Calculator.divide(a: 1, b: 0)
            print(result)
        }
        catch MathematicalError.zeroDivideError {
            debugPrint("MathematicalError.zeroDivideError")
        }
        catch {
            debugPrint("unexpected error")
        }
        defer {
            debugPrint("defer")
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    

}

