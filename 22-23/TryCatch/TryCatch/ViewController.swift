//
//  ViewController.swift
//  TryCatch
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        errorHandling1("dada")
        //errorHandling2(param: -6)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func errorHandling1(_ param: Any) {
        do {
            let myObject = try MyObject(param: param)
            debugPrint("Object created successfully wth id: \(myObject.identifier)")
        }
        catch let error as ObjectIdentifierLogicalError {
            switch error {
            case .zeroValueError: debugPrint("catch - zeroValueError")
            case .negativeValueError: debugPrint("catch - negativeValueError")
            case .maximumValueLimitError(let reason): debugPrint("catch - maximumValueLimitError -  \(reason)")
            }
        }
        catch let error as ObjectIdentifierCastError {
            debugPrint("catch - other error: \(error.localizedDescription)")
        }
        catch {
            debugPrint("unexpected error: \(error.localizedDescription)")
        }
    }
    
    func errorHandling2(param: Any) {
        do {
            let myObject = try MyObject(param: param)
            debugPrint("Object created successfully wth id: \(myObject.identifier)")
        }
        catch ObjectIdentifierCastError.integerCastError {
            debugPrint("catch - integerCastError. ")
        }
        catch ObjectIdentifierLogicalError.maximumValueLimitError(let reason) {
            debugPrint("catch - maximumValueLimitError - \(reason)")
        }
        catch ObjectIdentifierLogicalError.negativeValueError {
            debugPrint("catch - negativeValueError")
        }
        catch ObjectIdentifierLogicalError.zeroValueError {
            debugPrint("catch - zeroValueError")
        }
        catch {
            debugPrint("Unexpected error: \(error).")
        }
    }

}

