//
//  ViewController.swift
//  ApplicationStates
//
//  Created by Andras Somlai
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let coverView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willresignActive), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func willEnterForeground() {
        coverView.backgroundColor = UIColor.green
    }
    
    @objc func didBecomeActive() {
        coverView.removeFromSuperview()
    }

    @objc func willresignActive() {
        coverView.frame = view.bounds
        coverView.backgroundColor = UIColor.orange
        view.addSubview(coverView)
    }
    
    @objc func didEnterBackground() {
        coverView.backgroundColor = UIColor.red
    }

}

