//
//  DataCenter.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum RequiredDataError: Error, LocalizedError {
    case requiredDataIsNil
    var errorDescription: String? {
        return "Error: A required variable is nil"
    }
}

class DataCenter: NSObject {
    static let sharedInstance: DataCenter = {
        let instance = DataCenter()
        return instance
    }()
    private override init() { super.init() }
    
    private(set) var humans : [Human] = []
    
    func getHumans(completion : @escaping (Bool) -> Void) {
        NetworkManager.getPeople { (jsonData, error) in
            if let safeData = jsonData {
                self.humans.removeAll()
                for dict in safeData {
                    let newHuman = Human(dictionary: dict)
                    self.humans.append(newHuman)
                }
                completion(true)
            }
            else {
                completion(false)
            }
        }
    }
    
    
    //MARK: - Plist
    func saveToPlist() {
        let encoder = PropertyListEncoder()
        let filePath =  FileSystemHandler.getFilePath(withFileName: "csodamappa/MyPlist.plist")
        do {
            let data : Data = try encoder.encode(humans)
            try data.write(to:filePath)
        } catch {
            print("Error encoding notes, \(error)")
        }
    }
    
    //MARK: - JSON file
    func saveToJSON() {
        //enkódolásnak felel meg, menthetővé "alakítom" a custom objektumomat
        var humansDictionaries : [[String:Any]] = []
        for human in humans {
            humansDictionaries.append(human.getAsDictionary())
        }
        //enkódolás idág, encoded objektum: humansDictionaries
        let filePath = FileSystemHandler.getFilePath(withFileName: "MyJSON.json")
        
        do {
            //humansDictionaries-t data-vá alakítom
            let jsonData = try JSONSerialization.data(withJSONObject: humansDictionaries, options: .prettyPrinted)
            
            //data-t  mentem a fájlba
            try jsonData.write(to: filePath)
            
            //Stringgé is alakíthatokm még a data-t, és ezt is menthetem fájlba
            //let jsonString = String(data: jsonData, encoding: .utf8)!
            //try jsonString.write(to: filePath, atomically: false, encoding: .utf8)
            print("log")
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    func loadFromPlist() {
        humans.removeAll()

        let decoder = PropertyListDecoder()
        do {
            let data = try Data(contentsOf: FileSystemHandler.getFilePath(withFileName: "csodamappa/MyPlist.plist"))
            humans = try decoder.decode([Human].self, from: data)
        }
        catch {
            print("Error decoding notes, \(error)")
        }
        
        return
        
//   Szörny szintaktikával
//        if let data = try? Data(contentsOf: FileSystemHandler.getFilePath(withFileName: "MyPlist.plist")) {
//            do {
//                humans = try decoder.decode([Human].self, from: data)
//            } catch {
//                print("Error decoding notes, \(error)")
//            }
//        }
    }
    
    func loadFromJSON() {
        humans.removeAll()
        do {
            let fileData = try Data(contentsOf: FileSystemHandler.getFilePath(withFileName: "MyJSON.json"))
            //let fileDataString = try String(contentsOf: filePath(with: "MyJSON.json"), encoding: .utf8)
            let decodedJSON = try JSONSerialization.jsonObject(with: fileData, options: [])
            if let dictArrayFromJSON = decodedJSON as? [[String:Any]] {
                for dict in dictArrayFromJSON {
                    let newHuman = Human(dictionary: dict)
                    self.humans.append(newHuman)
                }
            }
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    //MARK: - Defaults
    func saveToDefaults() {
//        EAZY megoldás
//        var humansDictionaries : [[String:Any]] = []
//        for human in humans {
//            humansDictionaries.append(human.getAsDictionary())
//        }
//        UserDefaults.standard.set(humansDictionaries, forKey: "humans")
        
        let encodedData : Data = NSKeyedArchiver.archivedData(withRootObject: humans)
        UserDefaults.standard.set(encodedData, forKey: "humans")
    }
    
    func loadFromDefaults() {
        humans.removeAll()

        guard let loadedData = UserDefaults.standard.object(forKey: "humans") as? Data else {
            return
        }

        if let loadedHumans = NSKeyedUnarchiver.unarchiveObject(with: loadedData) as? [Human] {
            humans = loadedHumans
        }
    }
    
    
}
