//
//  Human.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum Gender : String, Codable {
    case male = "Male"
    case female = "Female"
}

class Human: NSObject, Codable, NSCoding {
    private(set) var id : Int?
    private(set) var firstName : String?
    private(set) var lastName : String?
    private(set) var email : String?
    private(set) var gender : Gender?
    private(set) var ipAddress : String?
    
    //standard init
    init(dictionary: [String : Any]) {
        id = dictionary["id"] as? Int
        firstName = dictionary["first_name"] as? String
        lastName = dictionary["last_name"] as? String
        email = dictionary["email"] as? String
        ipAddress = dictionary["ip_address"] as? String
        if let genderString = dictionary["gender"] as? String {
            gender = Gender(rawValue: genderString)
        }
    }
    
    func getAsDictionary() -> [String : Any] {
        var returnDict : [String : Any] = [:]
        
        returnDict["id"] = id
        returnDict["first_name"] = firstName
        returnDict["last_name"] = lastName
        returnDict["email"] = email
        returnDict["ip_address"] = ipAddress
        returnDict["gender"] = gender?.rawValue
        
        return returnDict
    }
    
    //Codable - For json parsing
    private enum CodingKeys : String, CodingKey {
        case id
        case firstName// = "first_name"
        case lastName// = "last_name"
        case email
        case gender
        case ipAddress// = "ip_address"
    }
    
    //NSCoding protocol - it's required to inherit from NSObject - for defaults saving
    required init(coder aDecoder: NSCoder) {
        let genderString = aDecoder.decodeObject(forKey: "gender") as? String ?? "Male"
        gender = Gender(rawValue: genderString)
        id = aDecoder.decodeObject(forKey: "id") as? Int
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        ipAddress = aDecoder.decodeObject(forKey: "ipAddress") as? String
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(gender?.rawValue, forKey: "gender")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(ipAddress, forKey: "ipAddress")
        aCoder.encode(id, forKey: "id")
    }


    
    
    
}
