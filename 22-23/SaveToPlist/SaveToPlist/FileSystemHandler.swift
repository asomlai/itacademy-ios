//
//  FileSystemHandler.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.


// /documents/kepek/myJSON.json

import UIKit

class FileSystemHandler: NSObject {
    
    static var documentsPath : String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    static func getFilePath(withFileName fileName : String) -> URL {
        let filePath = FileSystemHandler.documentsPath + "/" + fileName
        debugPrint("File Path: \(filePath)")
        return URL(fileURLWithPath: filePath)
    }
    
    static func createFolder(name: String) {
        let folderPath = FileSystemHandler.documentsPath + "/" + name
        let logsPath = NSURL(fileURLWithPath: folderPath)
//
//        let documentsPathUrl = NSURL(fileURLWithPath: FileSystemHandler.documentsPath)
//        let logsPath = documentsPathUrl.appendingPathComponent(name)!
        print("create folder at path: \(logsPath)")
        do {
            try FileManager.default.createDirectory(atPath: logsPath.path!, withIntermediateDirectories: true, attributes: nil)
        }
        catch {
            NSLog("Unable to create directory \(error)")
        }

    }
    
    static func getDocumentsDirectoryContent() -> [String] {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            return fileURLs.map{$0.absoluteString}
        }
        catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
            return []
        }
    }
    
}
