//
//  ViewController.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var folderNameTextField: UITextField!
    @IBOutlet weak var folderContentsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        folderNameTextField.delegate = self
        DataCenter.sharedInstance.getHumans { (success) in
            if(success) {
//                print(DataCenter.sharedInstance.humans)
                
                DataCenter.sharedInstance.saveToPlist()
                DataCenter.sharedInstance.loadFromPlist()
                print(DataCenter.sharedInstance.humans)
//
//                DataCenter.sharedInstance.saveToDefaults()
//                DataCenter.sharedInstance.loadFromDefaults()
//                print(DataCenter.sharedInstance.humans)
//
//                DataCenter.sharedInstance.saveToJSON()
//                DataCenter.sharedInstance.loadFromJSON()
//                print(DataCenter.sharedInstance.humans)
            }
        }
        
//        DataCenter.sharedInstance.getHumans(completion: handleGetHumansResult)
        
//        DataCenter.sharedInstance.getHumans { (success) in
//
//        }
    }
    
    func handleGetHumansResult(success: Bool) {
        if(success) {
            DataCenter.sharedInstance.saveToPlist()
            DataCenter.sharedInstance.loadFromPlist()
            print(DataCenter.sharedInstance.humans)
            
            //                DataCenter.sharedInstance.saveToDefaults()
            //                DataCenter.sharedInstance.loadFromDefaults()
            //                print(DataCenter.sharedInstance.humans)
            
            //                DataCenter.sharedInstance.saveToJSON()
            //                DataCenter.sharedInstance.loadFromJSON()
            //                print(DataCenter.sharedInstance.humans)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func createFolder(_ sender: UIButton) {
        FileSystemHandler.createFolder(name: folderNameTextField.text!)
    }
    
    @IBAction func update(_ sender: UIButton) {
        folderContentsTextView.text = ""
        for string in FileSystemHandler.getDocumentsDirectoryContent() {
            folderContentsTextView.text = folderContentsTextView.text + "\(string) \n\n"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}

