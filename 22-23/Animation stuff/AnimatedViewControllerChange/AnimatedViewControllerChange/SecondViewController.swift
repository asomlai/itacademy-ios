//
//  SecondViewController.swift
//  AnimatedViewControllerChange
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var squareTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var squareLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var squareRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.alpha = 0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let squareHeight : CGFloat = 150
        squareTopConstraint.constant = (view.frame.height) / 2 - squareHeight
        squareLeftConstraint.constant = (view.frame.width - squareHeight) / 2
        squareRightConstraint.constant = (view.frame.width - squareHeight) / 2
        UIView.animate(withDuration: 3, delay: 0.1, options: .allowUserInteraction, animations: {
            self.view.layoutIfNeeded()
        }) { (done) in
            UIView.animate(withDuration: 1, animations: {
                //animációk
                self.label.alpha = 1
                self.label.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            }) { (success) in
                //ha végzett
                let alert = UIAlertController(title: "Alert", message: "message", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        
    }

}
