//
//  ViewController.swift
//  ConstraintAnimations
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var pizzaHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pizzaWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var pizzaVerticalSpacing: NSLayoutConstraint!
    
    @IBOutlet weak var noPizzaImage: UIImageView!
    
    @IBOutlet weak var pizza: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func changeSize(_ sender: UIButton) {

        pizzaHeightConstraint.constant = noPizzaImage.frame.height / 2
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        }) { (_) in

            self.pizzaWidthConstraint.constant = self.noPizzaImage.frame.width / 2
            UIView.animate(withDuration: 1) {
                self.view.layoutIfNeeded()
                self.pizza.alpha = 0
            }
        }
        
//        pizzaHeightConstraint.constant = noPizzaImage.frame.height / 2
//        UIView.animate(withDuration: 1) {
//            self.view.layoutIfNeeded()
//        }
        
//        self.pizzaWidthConstraint.constant = self.noPizzaImage.frame.width / 2
//        UIView.animate(withDuration: 1, delay: 0.25, options: [.allowUserInteraction], animations: {
//            self.view.layoutIfNeeded()
//        }) { (_) in
//
//        }
        
//        self.pizzaWidthConstraint.constant = self.noPizzaImage.frame.width / 2
//        UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 10, options: [], animations: {
//            self.view.layoutIfNeeded()
//        }) { (_) in
//
//        }
        
        
//        UIView.animate(withDuration: 1, animations: {
//            self.view.layoutIfNeeded()
//        }) { (done) in }
        
        
//        pizzaWidthConstraint.constant = self.noPizzaImage.frame.width / 2
//        UIView.animate(withDuration: 1, delay: 0.5, options: .allowUserInteraction, animations: {
//            self.view.layoutIfNeeded()
//        }) { (done) in
            //notihing to do here
//        }
        
        
        
//        UIView.animate(withDuration: 1) {
//            self.pizza.frame = CGRect(x:0, y:0, width:50, height:50)
//            self.noPizzaImage.frame = CGRect(x:0, y:0, width:60, height:60)
//        }
        
    }
    
    @IBAction func changePosition(_ sender: UIButton) {
        pizzaVerticalSpacing.constant = -noPizzaImage.frame.height
//        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
//        })
    }
    
    @IBAction func reset(_ sender: UIButton) {
        pizzaHeightConstraint.constant = 0
        pizzaWidthConstraint.constant = 0
        pizzaVerticalSpacing.constant = 0
        self.pizza.alpha = 1
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

