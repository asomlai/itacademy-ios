//
//  ViewController.swift
//  ProgramaticallyConstraints
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let redView = UIView()
    let blueView = UIView()
    let yellowView = UIView()
    let brownView = UIView()
    
    var redWidthConstraint : NSLayoutConstraint!
    var redHeightConstraint : NSLayoutConstraint!
    var redWidthConstraint2 : NSLayoutConstraint!
    var redHeightConstraint2 : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Ha kódból írsz constrainteket, ezeket állítsd be false-ra
        redView.translatesAutoresizingMaskIntoConstraints = false
        blueView.translatesAutoresizingMaskIntoConstraints = false
        yellowView.translatesAutoresizingMaskIntoConstraints = false
        brownView.translatesAutoresizingMaskIntoConstraints = false
        
        redView.backgroundColor = UIColor.red
        blueView.backgroundColor = UIColor.blue
        yellowView.backgroundColor = UIColor.yellow
        brownView.backgroundColor = UIColor.brown
        
        view.addSubview(redView)
        view.addSubview(blueView)
        view.addSubview(yellowView)
        redView.addSubview(brownView)
        
        //Piros függőlegesen legyen középen
        NSLayoutConstraint(item: redView,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: view,
                           attribute: .centerX,
                           multiplier: 1,
                           constant: 0).isActive = true
        
        //Piros vízszintesen legyen középen
        NSLayoutConstraint(item: redView,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: view,
                           attribute: .centerY,
                           multiplier: 1,
                           constant: 0).isActive = true
        
        //Piros szélessége legyen egyenlő a view rövidebbik oldalának 0.5szörösével
        //2 constraintet kell ehhez hozzáadni. Mindig az lesz aktív, amelyiknek annak kell lennie
        redWidthConstraint = NSLayoutConstraint(item: redView,
                                                attribute: .width,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: .width,
                                                multiplier: 0.5,
                                                constant: 0)
        
        redWidthConstraint2 = NSLayoutConstraint(item: redView,
                                                attribute: .width,
                                                relatedBy: .equal,
                                                toItem: view,
                                                attribute: .height,
                                                multiplier: 0.5,
                                                constant: 0)
        
        //Piros magassága legyen egyenlő a view rövidebbik oldalának 0.5szörösével
        //2 constraintet kell ehhez hozzáadni. Mindig az lesz aktív, amelyiknek annak kell lennie
        redHeightConstraint = NSLayoutConstraint(item: redView,
                                                 attribute: .height,
                                                 relatedBy: .equal,
                                                 toItem: view,
                                                 attribute: .width,
                                                 multiplier: 0.5,
                                                 constant: 0)
        
        redHeightConstraint2 = NSLayoutConstraint(item: redView,
                                                 attribute: .height,
                                                 relatedBy: .equal,
                                                 toItem: view,
                                                 attribute: .height,
                                                 multiplier: 0.5,
                                                 constant: 0)
        
        //Indításkori aktív constraintek kiválasztása
        if(view.frame.width < view.frame.height) {
            redHeightConstraint.isActive = true
            redHeightConstraint2.isActive = false
            redWidthConstraint.isActive = true
            redWidthConstraint2.isActive = false
        }
        else {
            redHeightConstraint.isActive = false
            redHeightConstraint2.isActive = true
            redWidthConstraint.isActive = false
            redWidthConstraint2.isActive = true
        }
        
        //Kék magasága legyen 50 pixel
        NSLayoutConstraint(item: blueView,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1,
                           constant: 50).isActive = true

        //Kék x pizíciója legyen azonos piros x pizíciójával
        NSLayoutConstraint(item: blueView,
                           attribute: .leading,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .leading,
                           multiplier: 1,
                           constant: 0).isActive = true

        //Kék szélessége legyen: piros szélességének a fele - 5px
        NSLayoutConstraint(item: blueView,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .width,
                           multiplier: 0.5,
                           constant: -5).isActive = true

        //Kék teteje legyen a piros aljától 10 pixel távolságra
        NSLayoutConstraint(item: blueView,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .bottom,
                           multiplier: 1,
                           constant: 10).isActive = true
        
        //Sárga magasága legyen 50 pixel
        NSLayoutConstraint(item: yellowView,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: nil,
                           attribute: .notAnAttribute,
                           multiplier: 1,
                           constant: 50).isActive = true
        
        //Sárga x pozíciója legyen kék jobb szélétől 10 pixelre
        NSLayoutConstraint(item: yellowView,
                           attribute: .leading,
                           relatedBy: .equal,
                           toItem: blueView,
                           attribute: .trailing,
                           multiplier: 1,
                           constant: 10).isActive = true
        
        //Sárga szélessége legyen: piros szélességének a fele - 5px
        NSLayoutConstraint(item: yellowView,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .width,
                           multiplier: 0.5,
                           constant: -5).isActive = true
        
        //Sárga teteje legyen pontosan ott, ahol kék teteje
        NSLayoutConstraint(item: yellowView,
                           attribute: .top,
                           relatedBy: .equal,
                           toItem: blueView,
                           attribute: .top,
                           multiplier: 1,
                           constant: 0).isActive = true
        
        //Barna magasága legyen piros magasságának felével
        NSLayoutConstraint(item: brownView,
                           attribute: .height,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .height,
                           multiplier: 0.5,
                           constant: 0).isActive = true
        
        //Barna szélessége legyen piros szélességének felével
        NSLayoutConstraint(item: brownView,
                           attribute: .width,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .width,
                           multiplier: 0.5,
                           constant: 0).isActive = true
        
        //Barna függőleges közepe legyen piros függőleges közepénél
        NSLayoutConstraint(item: brownView,
                           attribute: .centerX,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .centerX,
                           multiplier: 1,
                           constant: 0).isActive = true
        
        //Barna vízszintes közepe legyen piros vízszintes közepénél
        NSLayoutConstraint(item: brownView,
                           attribute: .centerY,
                           relatedBy: .equal,
                           toItem: redView,
                           attribute: .centerY,
                           multiplier: 1,
                           constant: 0).isActive = true
        
    }
    
    
    //Ez a függvény akor hívódik meg, ha a sizeClass változott (tehát pl forgatásnál)
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        //Size class logolása
        switch (self.traitCollection.horizontalSizeClass, self.traitCollection.verticalSizeClass) {
        case (.unspecified, .unspecified):
            print("Unknown")
        case (.unspecified, .compact):
            print("Unknown width, compact height")
        case (UIUserInterfaceSizeClass.unspecified, UIUserInterfaceSizeClass.regular):
            print("Unknown width, regular height")
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.unspecified):
            print("Compact width, unknown height")
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.unspecified):
            print("Regular width, unknown height")
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.compact):
            print("Regular width, compact height")
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.compact):
            print("Compact width, compact height")
        case (UIUserInterfaceSizeClass.regular, UIUserInterfaceSizeClass.regular):
            print("Regualr width, regular height")
        case (UIUserInterfaceSizeClass.compact, UIUserInterfaceSizeClass.regular):
            print("Compact width, regular height")
        }
        
        //Constraintek frissítése
        if(view.frame.width < view.frame.height) {
            redHeightConstraint.isActive = true
            redHeightConstraint2.isActive = false
            redWidthConstraint.isActive = true
            redWidthConstraint2.isActive = false
        }
        else {
            redHeightConstraint.isActive = false
            redHeightConstraint2.isActive = true
            redWidthConstraint.isActive = false
            redWidthConstraint2.isActive = true
        }
    }

}

