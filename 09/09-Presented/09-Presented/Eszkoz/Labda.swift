//
//  Labda.swift
//  09-Presented
//
//  Created by Andras Somlai on 2018. 10. 11..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Labda: Targy {
    
    var minimalisTerfogat: Double = 0
    
    init(ujTomeg: Double, ujTerfogat: Double, ujMinimalisTerfogat: Double) {
        super.init()
        tomeg = ujTomeg
        terfogat = ujTerfogat
        minimalisTerfogat = ujMinimalisTerfogat
    }
    
    override func getTomeg() -> Double {
        let ujTerfogat = terfogat - 1
        if(ujTerfogat >= minimalisTerfogat) {
            terfogat = ujTerfogat
        }
        else {
            terfogat = minimalisTerfogat
        }
        
        return tomeg
    }
    
}
