//
//  Uto.swift
//  09-Presented
//
//  Created by Andras Somlai on 2018. 10. 11..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Uto: Targy {

    init(ujTomeg: Double, ujTerfogat: Double) {
        super.init()
        tomeg = ujTomeg
        terfogat = ujTerfogat
    }
}
