//
//  Targy.swift
//  09-Presented
//
//  Created by Andras Somlai on 2018. 10. 11..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Targy: NSObject {
    var tomeg : Double = 0
    var terfogat : Double = 0
    
    func getTomeg() -> Double {
        return tomeg
    }
    
    func getTerfogat() -> Double {
        return terfogat
    }
    
}
