//
//  main.swift
//  09-Presented
//
//  Created by Andras Somlai on 2018. 10. 11..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Foundation

//http://suranyisz.hu/infoA/programozasi_alapfeladatok.pdf

//1
//print("hello word")

//3
//var szam: Int = 4
//var szamKetszerese = szam*2
//print(szamKetszerese)


//4
//var a4: Int = 45
//var b4: Int = 65
//
//var osszeguk4 = a4 + b4
//var kulonbseguk4 = a4 - b4
//var pozitivKulonbseg = abs(kulonbseguk4)
//var szorzatuk4 = a4 * b4
//var hanyadosuk4: Double = -1
//if(b4 != 0) {
//    hanyadosuk4 = Double(a4)/Double(b4)
//}
//
//let mitFogunkKiirni: String = "hanyadosuk:\(hanyadosuk4), osszguk:\(osszeguk4), szorzatuk: \(szorzatuk4), kulonbseguk: \(kulonbseguk4), abszolutKulonbseg: \(pozitivKulonbseg)"
//
//print(mitFogunkKiirni)

//5
//var a5 = 7
//var b5 = 6
//
//if(a5 > b5) {
//    print("\(a5) nagyobb, mint \(b5)")
//}
//else if(b5 > a5) {
//    print("\(b5) nagyobb, mint \(a5)")
//}
//else {
//    print("Egyenlőek")
//}
//
//func irdKiANagyobbat(a: Int, b: Int) {
//    if(a > b) {
//        print("\(a) nagyobb, mint \(b)")
//        return
//    }
//    else if(b > a) {
//        print("\(b) nagyobb, mint \(a)")
//    }
//    else {
//        print("Egyenlőek")
//    }
//}
//
//irdKiANagyobbat(a: 5, b: 5)


//7
//var a7: Double = 1
//var b7: Double = 6.1
//var c7: Double = 7
//
//if((a7 + b7 > c7) && (b7 + c7 > a7) && (c7 + a7 > b7)) {
//    print("háromszög szerkeszthető")
//}
//else {
//    print("háromszög NEM szerkeszthető")
//}

//15
//func irdKiEddig(szam: Int) {
//    if (szam < 0) {
//        print("A függvény bemenete helytelen")
//        return
//    }
// Egymás alá írja
//    for i in stride(from: 0, through: szam, by: 1) {
//        print(i)
//    }
    
//    Egymás mellé írja
//    var szamok = ""
//    for i in stride(from: 0, through: szam, by: 1) {
//        szamok = szamok + "\(i), "
//    }
//
//    print(szamok)
//}
//
//irdKiEddig(szam: 10)
//
//
//func irdKiAzAzonosSzamjegyueket() {
//    for i in stride(from: 11, to: 101, by: 1) {
//        //print("FUT A CIKLUS")
//        if(i % 11 == 0) {
//            print(i)
//        }
//    }
//}
//
//func irdKiAzAzonosSzamjegyueketOptimalis() {
//    for i in stride(from: 11, to: 101, by: 11) {
//        //print("FUT A CIKLUS")
//        print(i)
//    }
//}
//
////irdKiAzAzonosSzamjegyueketOptimalis()
//
//
//func addVisszaAzAzonosSzamjegyueketOptimalis() -> [Int] {
//    var tomb: [Int] = []
//    for i in stride(from: 11, to: 101, by: 11) {
//        tomb.append(i)
//    }
//    return tomb
//}
//
//let szamoltTomb = addVisszaAzAzonosSzamjegyueketOptimalis()
//print(szamoltTomb[3])
//
//print(addVisszaAzAzonosSzamjegyueketOptimalis()[3])
//
//
//
//func irdKiANagyobbat2(a: Int, b: Int) {
//    if(a > b) {
//        print("\(a) nagyobb, mint \(b)")
//        return
//    }
//    else if(b > a) {
//        print("\(b) nagyobb, mint \(a)")
//    }
//    else {
//        print("Egyenlőek")
//    }
//}
//irdKiANagyobbat2(a: 3, b: 6)
//
//
//func irdKiANagyobbat3(_ a: Int, _ b: Int) {
//    if(a > b) {
//        print("\(a) nagyobb, mint \(b)")
//        return
//    }
//    else if(b > a) {
//        print("\(b) nagyobb, mint \(a)")
//    }
//    else {
//        print("Egyenlőek")
//    }
//}
//irdKiANagyobbat3(3, 6)
//
//
//func irdKiANagyobbat4(egyik a: Int, masik b: Int) {
//    if(a > b) {
//        print("\(a) nagyobb, mint \(b)")
//        return
//    }
//    else if(b > a) {
//        print("\(b) nagyobb, mint \(a)")
//    }
//    else {
//        print("Egyenlőek")
//    }
//}
//
//irdKiANagyobbat4(egyik: 3, masik: 6)
//
//
//func atlag(tomb: [Int]) -> Double {
//    var elemszam = tomb.count
//    var sum = 0
//    for i in stride(from: 0, to: tomb.count, by: 1) {
//        let aktualisElem = tomb[i]
//        print("i értéke: \(i), aktualisElem: \(aktualisElem)")
//        sum = sum + aktualisElem
//    }
//
//    sum = 0
//    for aktualisElem in tomb {
//        print("aktualisElem: \(aktualisElem)")
//        sum = sum + aktualisElem
//    }
//
//    var atlag = Double(sum)/Double(elemszam)
//    return atlag
//}
//
//print(atlag(tomb: [11,23]))



//Osztályok

var labda = Labda(ujTomeg: 23, ujTerfogat: 10, ujMinimalisTerfogat: 1)
print("labda terfogata: \(labda.getTerfogat())")
print("labda tomege: \(labda.getTomeg())")
print("labda terfogata2: \(labda.getTerfogat())")

var uto = Uto(ujTomeg: 5, ujTerfogat: 3)
print("utő térfogata:\(uto.getTerfogat())")
