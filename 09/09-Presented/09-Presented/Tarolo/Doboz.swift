//
//  Doboz.swift
//  09-Presented
//
//  Created by Andras Somlai on 2018. 10. 11..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Doboz: Targy {
    var taroltDolgok : [Targy] = []
    
    
    func berak(targy: Targy) {
        var jelenlegiTargyakOsszerfogat: Double = 0
        for aktualisTargy in taroltDolgok {
            jelenlegiTargyakOsszerfogat += aktualisTargy.getTerfogat()
        }
        
        if(jelenlegiTargyakOsszerfogat + targy.getTerfogat() > terfogat) {
            print("Nem lehet betenni")
            return
        }
        
        taroltDolgok.append(targy)
    }
    
    func getOssztomeg() -> Double {
        var targyakOssztomeg: Double = 0
        for aktualisTargy in taroltDolgok {
            targyakOssztomeg += aktualisTargy.getTomeg()
        }
        return targyakOssztomeg
    }
    
}
