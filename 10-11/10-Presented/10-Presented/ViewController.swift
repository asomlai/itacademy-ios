//
//  ViewController.swift
//  10-Presented
//
//  Created by Andras Somlai on 2018. 10. 16..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var allatInfoTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allatInfoTextView.text = "Ide írom majd az infot"
    }

    @IBAction func toroldAzUtolsot(_ sender: Any) {
        allatInfoTextView.text = "TÖRÖLVE"
    }
    
    @IBAction func toroldAzOsszeset(_ sender: Any) {
        allatInfoTextView.text = "ÖSSZES TÖRÖLVE"
    }
}

