//
//  NewPetViewController.swift
//  10-Presented
//
//  Created by Andras Somlai on 2018. 10. 16..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum AllatNem: Int {
    case nosteny = 0
    case him = 1
    case ismeretlen = 2
}

class NewPetViewController: UIViewController {
    @IBOutlet weak var eletkorLabel: UILabel!
    @IBOutlet weak var nevTextField: UITextField!
    @IBOutlet weak var jofejsegLabel: UILabel!
    @IBOutlet weak var nemSegmentedControl: UISegmentedControl!
    @IBOutlet weak var eletkorStepperOutlet: UIStepper!
    @IBOutlet weak var visszajelzesLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        visszajelzesLabel.isHidden = true
        // Do any additional setup after loading the view.
    }

    
    @IBAction func jofejsegValtozott(_ sender: UISlider) {
        jofejsegLabel.text = String(sender.value)
    }
    
    @IBAction func eletkorValtozott(_ sender: UIStepper) {
        print(sender.value)
        let intEletkor = Int(sender.value)
        let eletkorString = String(intEletkor)
        eletkorLabel.text = "Az állat " + eletkorString + " éves"
    }
    
    @IBAction func vissza(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func allatHozzaadasa(_ sender: Any) {
        visszajelzesLabel.isHidden = false
        let allatNeme = AllatNem(rawValue: nemSegmentedControl.selectedSegmentIndex)
        
        print("Neve: \(nevTextField.text), állat jófejsége: \(jofejsegLabel.text), állat neme: \(allatNeme), állat életkora: \(eletkorStepperOutlet.value)")
    }
    
    
}
