//
//  Allat.swift
//  10-Presented
//
//  Created by Andras Somlai on 2018. 10. 18..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Allat {
    var nev : String?
    var neme : AllatNem?
    var jofejeg : Double?
    var eletkor : Int?
    
    func getAsString() -> String {
        var allatNeme = ""
        if(neme == nil) {
            neme = .ismeretlen
        }
        
        switch neme! {
        case .nosteny: allatNeme = "Nosteny"
        case .him: allatNeme = "Hím"
        case .ismeretlen: allatNeme = "Ismeretlen"
        }
        
        let ismeretlenString = "ismeretlen"
        return "Az állat neve: \(nev ?? ismeretlenString), az állat jófejsége: \(String(jofejeg ?? 0)), az állat életkora: \(String(eletkor ?? 0)), Az állat neme: \(allatNeme)"
    }
}
