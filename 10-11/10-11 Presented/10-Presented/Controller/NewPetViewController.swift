//
//  NewPetViewController.swift
//  10-Presented
//
//  Created by Andras Somlai on 2018. 10. 16..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum AllatNem: Int {
    case nosteny = 0
    case him = 1
    case ismeretlen = 2
}

class NewPetViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var eletkorLabel: UILabel!
    @IBOutlet weak var nevTextField: UITextField!
    @IBOutlet weak var jofejsegLabel: UILabel!
    @IBOutlet weak var nemSegmentedControl: UISegmentedControl!
    @IBOutlet weak var eletkorStepperOutlet: UIStepper!
    @IBOutlet weak var visszajelzesLabel: UILabel!
    
    var szerkesztettAllat: Allat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        visszajelzesLabel.isHidden = true
        nevTextField.delegate = self
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func jofejsegValtozott(_ sender: UISlider) {
        jofejsegLabel.text = String(sender.value)
    }
    
    @IBAction func eletkorValtozott(_ sender: UIStepper) {
        print(sender.value)
        let intEletkor = Int(sender.value)
        let eletkorString = String(intEletkor)
        eletkorLabel.text = "Az állat " + eletkorString + " éves"
    }
    
    @IBAction func vissza(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func allatHozzaadasa(_ sender: Any) {
        visszajelzesLabel.isHidden = false
        let allatNeme = AllatNem(rawValue: nemSegmentedControl.selectedSegmentIndex)
        
        
        szerkesztettAllat.nev = nevTextField.text
        //ujAllat.jofejeg = Double(jofejsegLabel.text ?? "0")
        szerkesztettAllat.jofejeg = Double(jofejsegLabel.text!)
        szerkesztettAllat.neme = allatNeme
        szerkesztettAllat.eletkor = Int(eletkorStepperOutlet.value)
        
        dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    
    
}
