//
//  ViewController.swift
//  10-Presented
//
//  Created by Andras Somlai on 2018. 10. 16..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var allatInfoTextView: UITextView!
    var allatok : [Allat] = []
    
    
    //Betöltődés
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.blue
        allatInfoTextView.text = "Ide írom majd az infot"
    }
    
    //Képernyőn való megjelenés
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = UIColor.yellow
        updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.backgroundColor = UIColor.lightGray
    }
    
    //Képernyőről való eltűnés
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.backgroundColor = UIColor.red
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        view.backgroundColor = UIColor.magenta
    }
    
    //Méretváltozás
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        view.backgroundColor = UIColor.green
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.backgroundColor = UIColor.purple
    }
    

    @IBAction func toroldAzUtolsot(_ sender: Any) {
        allatok.removeLast()
        updateUI()
    }
    
    @IBAction func toroldAzOsszeset(_ sender: Any) {
        allatok.removeAll()
        updateUI()
    }
    
    func updateUI() {
        allatInfoTextView.text = ""
        
        for allat in allatok {
            allatInfoTextView.text = allatInfoTextView.text + allat.getAsString() + "\n\n\n"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("override func prepare(for segue: UIStoryboardSegue, sender: Any?)")
        
        if(segue.identifier == "addPet") {
            let ujAllat = Allat()
            allatok.append(ujAllat)
            (segue.destination as! NewPetViewController).szerkesztettAllat = ujAllat
            
        }
    }
}

