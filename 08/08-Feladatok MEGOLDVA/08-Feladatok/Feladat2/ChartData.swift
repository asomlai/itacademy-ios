//
//  ChartData.swift
//  07-Proj
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ChartData {
    //Data default értéke lehet üres tömb. Így nem lesz nil, és az append-del lehet hozzáadni új elemeket
    var data : [CoordinateData] = []
    
    init() {
        for i in stride(from: -10, to: 10.1, by: 0.1) {
            //Kiszervezem változóba a dictionaryben lévő adatokat
            let xValue = i
            let yValue = xValue*xValue
            
            //Létrehozom az új CoordinateData típusú pontot. Info biztosan nem áll rendelkezésünkre, így az a paraméter nil.
            let point = CoordinateData(x: xValue, y: yValue, info: nil)
            
            //Hozzáadom a pontokat tartalmazó tömbhöz
            data.append(point)
        }
    }
    
    init(dataArray : [[String : Any]]) {
        //Végigiterálok a paraméterként megkapott sictionaryket tartalmazó tömbön
        //item [String : Any] típusú
        for item in dataArray {
            
            //Kiszervezem változóba a dictionaryben lévő adatokat
            let xValue = item["value"] as! Double
            let yValue = xValue*xValue
            let info = item["date_time"]
            
            //Létrehozom az új CoordinateData típusú pontot
            let point = CoordinateData(x: xValue, y: yValue, info: info)
            
            //Hozzáadom a pontokat tartalmazó tömbhöz
            data.append(point)
        }
    }
    
    func getMaxX() -> CoordinateData {
        //Ha a tömb üres, egy nullákat tartalmazó pontot adok vissza, hobakezelés céljából.
        if(data.count == 0) {
            return CoordinateData(x: 0, y: 0, info: nil)
        }
        
        //Standard maximum keresési logika, csak ezúttal struktúra egy adott értéke szerint keressük a maximum értéket
        var maxXPoint = data[0]
        for point in data {
            if(point.x > maxXPoint.x) {
                maxXPoint = point
            }
        }
        
        return maxXPoint
    }
    
    func getMaxY() -> CoordinateData {
        //Ha a tömb üres, egy nullákat tartalmazó pontot adok vissza, hobakezelés céljából.
        if(data.count == 0) {
            return CoordinateData(x: 0, y: 0, info: nil)
        }
        
        //Standard maximum keresési logika, csak ezúttal struktúra egy adott értéke szerint keressük a maximum értéket
        var maxYPoint = data[0]
        for point in data {
            if(point.y > maxYPoint.y) {
                maxYPoint = point
            }
        }
        
        return maxYPoint
    }
}
