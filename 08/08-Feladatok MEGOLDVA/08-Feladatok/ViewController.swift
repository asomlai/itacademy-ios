//
//  ViewController.swift
//  07-Proj
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var maxXLabel: UILabel!
    @IBOutlet weak var maxYLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "Number of elements in array: ?"
        textView.text = "Ide írasd majd ki a json-ben lévő elemeket szövegesen"
        textView.isEditable = false
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Ez a függvény adja vissza a json file tartalmát [[String : Any]] típusban
    func getCoordinateData() -> [[String : Any]] {
        if let filePath = Bundle.main.path(forResource: "chartData", ofType: "json") {
            let path=URL(fileURLWithPath: filePath)
            do {
                let fileContent = try Data(contentsOf: path)
                if let jsonArray = try JSONSerialization.jsonObject(with: fileContent, options : .allowFragments) as? [[String : Any]] {
                    return jsonArray
                } else {
                    print("bad json")
                }
            }
            catch {
                debugPrint(error.localizedDescription)
            }
        }
        return []
    }
    
    //FELADATMEGOLDÁSAITOK INNENTŐL
    @IBAction func loadDefaultDataButtonTouched(_ sender: Any) {
        let chartData = ChartData()
        label.text = "Number of elements in array: \(chartData.data.count)"
        
        //infoStringben lesznek benne sztringként az adatok
        var infoString = ""
        for point in chartData.data {
            infoString = infoString + "\n (\(point.xAsString) \(point.yAsString))-\(point.info ?? "")"
        }
        
        //infoString kiíratása a felhasználói felületre
        textView.text = infoString
        
        //Max pontok lekérdezése
        let maxXPoint = chartData.getMaxX()
        let maxYPoint = chartData.getMaxY()
        
        //Max pontok iíratása
        maxXLabel.text = "(\(maxXPoint.xAsString)-\(maxXPoint.yAsString))-\(maxXPoint.info ?? "")"
        maxYLabel.text = "(\(maxYPoint.xAsString)-\(maxYPoint.yAsString))-\(maxYPoint.info ?? "")"
    }
    
    @IBAction func loadDataFromJsonButtonTouched(_ sender: Any) {
        let arrayFromJson = getCoordinateData()
        let chartData = ChartData(dataArray: arrayFromJson)
        label.text = "Number of elements in array: \(chartData.data.count)"
        
        //infoStringben lesznek benne sztringként az adatok
        var infoString = ""
        for point in chartData.data {
            infoString = infoString + "\n (\(point.xAsString)-\(point.yAsString))-\(point.info ?? "")"
        }
        
        //infoString kiíratása a felhasználói felületre
        textView.text = infoString
        
        //Max pontok lekérdezése
        let maxXPoint = chartData.getMaxX()
        let maxYPoint = chartData.getMaxY()
        
        //Max pontok iíratása
        maxXLabel.text = "(Max X: \(maxXPoint.xAsString)-\(maxXPoint.yAsString))-\(maxXPoint.info ?? "")"
        maxYLabel.text = "(Max Y: \(maxYPoint.xAsString)-\(maxYPoint.yAsString))-\(maxYPoint.info ?? "")"
    }
    
    
    
}

