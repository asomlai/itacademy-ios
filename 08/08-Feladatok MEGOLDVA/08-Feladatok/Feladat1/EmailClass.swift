//
//  EmailClass.swift
//  08-Feladatok
//
//  Created by Somlai András on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class EmailClass {
    var mailbox: String
    var provider: String
    var domain: String
    
    //computed property
    var email : String {
        get {
            return "\(mailbox)@\(provider).\(domain)"
        }
    }
    
//    Ugyanúgy helyes megoldás
//    var email : String {
//        return "\(mailbox)@\(provider).\(domain)"
//    }
    
    init(newMailbox: String, newProvider: String, newDomain: String) {
        mailbox = newMailbox
        provider = newProvider
        domain = newDomain
    }
    
    func getEmailAddress() -> String {
        return "\(mailbox)@\(provider).\(domain)"
    }
}
