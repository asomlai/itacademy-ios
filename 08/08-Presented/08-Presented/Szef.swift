//
//  Szef.swift
//  08-Presented
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

struct Szef {

    private var kod : Int = 43252
    
    func nyilikE(beirtKod: Int) -> Bool {
        if(beirtKod == kod) {
            return true
        }
        else {
            return false
        }
        
    }
}
