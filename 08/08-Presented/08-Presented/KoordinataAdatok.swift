//
//  KoordinataAdatok.swift
//  08-Presented
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

struct KoordinataAdatok {
    var x: Double
    var y: Double
    var info: Any?
    
    init(newX: Double) {
        x = newX
        y = x*x
    }
}
