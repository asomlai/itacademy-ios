//
//  Haz.swift
//  08-Presented
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Haz: NSObject {
    var lakok : [No] = []
    
    
    func getOsszEletkor() -> Int {
        var osszEletkor = 0
        
        for aktualisEmber in lakok {
            osszEletkor += aktualisEmber.eletkor
        }
        
        return osszEletkor
    }
}
