//
//  Hangulat.swift
//  08-Presented
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

enum Hangulat : String {
    case szomoru
    case semleges
    case boldog
}
