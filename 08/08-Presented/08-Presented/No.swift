//
//  Ember.swift
//  08-Presented
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class No: NSObject {
    //MARK: - Statikus változó(k)
    static let iszikE : Bool = true
    
    //MARK: - Tárolt példány szintű változók
    var nev : String = ""
    var anyjaNeve : String = ""
    private var hangulat : Hangulat = .semleges {
        didSet {
            if(oldValue != hangulat) {
                megvaltozottAHangulata()
            }
        }
    }
    var szuletesiEv : Int = 1900
    var joTanulo : Bool = true
    var jofejekASzulei : Bool = false
    var elfogyasztottKavekSzama : Int = 0 {
        willSet {
            print("Az elfogyasztott kávék száma módosulni fog \(elfogyasztottKavekSzama)-ról \(newValue)-ra")
            
            //Ha pontosan 0
            if(newValue == 0) {
                hangulat = .semleges
            }
            //Ha <= 3, de nem 0
            else if(newValue <= 3) {
                hangulat = .boldog
            }
            //Minden egyéb esetben
            //Tehát, ha > 3
            else {
                hangulat = .szomoru
            }
        }
        didSet {
            print("Az elfogyasztott kávék száma módosult \(oldValue)-ról \(elfogyasztottKavekSzama)-ra")
        }
    }
    
    
    //MARK: - Computed változók
    var ihatEKavet : Bool {
        if(szuletesiEv < 2000 && joTanulo && jofejekASzulei) {
            return true
        }
        else {
            return false
        }
    }
    
    //Egyszerűsített szintaktika
//    var eletkor : Int {
//        return 2018 - szuletesiEv
//    }
    
    //Bővebb szintaktika
    var eletkor : Int {
        get {
            return 2018 - szuletesiEv
        }
        set {
            szuletesiEv = 2018 - newValue
        }
    }
    
    
    //MARK: - Függvények
    //Életkor lekérdezése függvénnyel
    func getEletkor() -> Int {
        return 2018 - szuletesiEv
    }
    
    //Életkor beállítása függvénnyel
    func setEletkor(newValue : Int) {
        szuletesiEv = 2018 - newValue
    }
    
    func setHangulat(newValue: Hangulat) {
        if(hangulat != newValue) {
            megvaltozottAHangulata()
        }
    }
    
    func veszEgyKiskutyat() {
        hangulat = .boldog
    }
    
    func megvaltozottAHangulata() {
        print("ffaefisegfsf yrgrgsrf vegfaesizvdybv yrv bivbsrsbfv yig bzcg vzuc bzurd bsbs dfsi gfs usfi fd dfi")
    }
    
    
    func eljonEVacsorazni() -> Bool {
        if(hangulat == .boldog) {
            return true
        }
        else {
            return false
        }
    }
}
