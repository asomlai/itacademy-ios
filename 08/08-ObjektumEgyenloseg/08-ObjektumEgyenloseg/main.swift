//
//  main.swift
//  08-ObjektumEgyenloseg
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Foundation

print("Hello, World!")


let kutya = Allat()
kutya.fajnev = "kutya"

let kutya2 = Allat()
kutya2.fajnev = "kutya"

let pok = Allat()
pok.fajnev = "pok"
pok.labakSzama = 8
pok.szemekSzama = 6

if(kutya == kutya2) {
    print("azonosak")
}
else {
    print("Nem azonosak")
}
