//
//  Allat.swift
//  08-ObjektumEgyenloseg
//
//  Created by Andras Somlai on 2018. 10. 09..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Allat: Equatable {
    
    
    var fajnev : String = "ismeretlen"
    var labakSzama : Int = 4
    var szemekSzama : Int = 2
    var tudERepulni : Bool = false
    
    static func == (lhs: Allat, rhs: Allat) -> Bool {
        if(lhs.fajnev == rhs.fajnev) {
            return true
        }
        return false
    }
}
