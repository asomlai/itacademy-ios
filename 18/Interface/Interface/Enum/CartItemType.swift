//
//  CartyItemType.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

enum CartItemType {
    case pasta
    case pizza
    case tortilla
    case food
    case drink
}
