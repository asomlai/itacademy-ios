//
//  Cart.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Cart {
    var items : [CartItem] = []
    
    func printCart() {
        for item in items {
            item.prettyPrint()
        }
    }
    
    func add(_ item: CartItem) {
        items.append(item)
    }
}
