//
//  Pizza.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Pizza : Food {
    static let basePrice : Double = 4
    override var price : Double {
        set {}
        get {
            var sumPrice = Pizza.basePrice
            for ingredient in ingredients {
                sumPrice += ingredient.price
            }
            return sumPrice
        }
    }
    var ingredients : [Food] = []
    
    //Nem lehet névvel és árral példányosítani
    override private init(name: String, price: Double) {
        super.init(name: name, price: price)
        type = .pizza
    }
    
    override init() {
        super.init()
        type = .pizza
    }
    
    func addIngredient(ingredient: Food) {
        ingredients.append(ingredient)
    }
    
    override func prettyPrint() {
        print("Pizza name: \(name), price:\(price)")
        print("Pizza Ingredient list:")
        for ingredient in ingredients {
            ingredient.prettyPrint()
        }
        print("End of Pizza Ingredient list:")
    }
    
}
