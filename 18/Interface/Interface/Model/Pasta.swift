//
//  Pasta.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Pasta: Food {
    var pastaType : PastaType = .spaghetti
    
    //nem lehet létzrehozni pasta objektumot így: let pasta = Pasta()
    override private init() {
        super.init()
        type = .pasta
    }
    //nem lehet létzrehozni pasta objektumot így: let pasta = Pasta(name: "Carbonara", price:10)
    override private init(name: String, price: Double) {
        super.init()
        type = .pasta
    }
    
    init(name: String, pastaType: PastaType, price: Double) {
        self.pastaType = pastaType
        super.init()
        type = .pasta
        self.price = price
    }
    
    override func prettyPrint() {
        print("Tortilla name: \(name), price:\(price), isHot:\(pastaType.rawValue)")
    }
}
