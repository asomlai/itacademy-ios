//
//  ViewController.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let cart = Cart()
        
        
        //let pasta = Pasta(name: "MyPasta", pastaType: .fusili, price: 12)
        //cart.add(pasta)
        
        let tortilla1 = Tortilla(name: "Csirkés", price: 6)
        let tortilla2 = Tortilla(name: "Mexikói", price: 6)
        tortilla2.isHot = true
        
        let cheese = Food(name: "Sajt", price: 2)
        let tomatoSauce = Food(name: "Paradicsomszósz", price: 0.5)
        let salami = Food(name: "Salami", price: 2.5)
        
        let pizza = Pizza()
        pizza.name = "Szalámis"
        pizza.addIngredient(ingredient: cheese)
        pizza.addIngredient(ingredient: tomatoSauce)
        pizza.addIngredient(ingredient: salami)
        
        cart.add(tortilla1)
        cart.add(tortilla2)
        cart.add(pizza)
        
        let cola = Drink(price: 6, name: "Coca cola")
        cart.add(cola)
        
        let lemonade = Drink(price: 0.5, name: "Anya limonádéja", alcoholic: true, producerName: "Anya")
        cart.add(lemonade)
        
        
        
        cart.printCart()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

