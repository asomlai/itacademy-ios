//
//  SecondViewController.swift
//  NotificationCenter
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var timerLabel : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateLabel()
        NotificationCenter.default.addObserver(self, selector: #selector(updateLabel), name: Notification.Name("milisecondsChanged"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func updateLabel() {
        let timer = TimerObject.sharedInstance
        timerLabel.text = "\(timer.minutesPart):\(timer.secondsPart):\(timer.millisecondsPart)"
    }
}
