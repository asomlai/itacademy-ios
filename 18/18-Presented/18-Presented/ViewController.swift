//
//  ViewController.swift
//  18-Presented
//
//  Created by Andras Somlai on 2018. 11. 20..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    var seconds = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateLabel()
        
        NotificationCenter.default.addObserver(self, selector: #selector(secondsChanged), name: Notification.Name("secondsChanged"), object: nil)
    }
    
    @objc func secondsChanged() {
        updateLabel()
    }
    
    func updateLabel() {
        label.text = String(Stopper.sharedInstance.seconds)
    }
    @IBAction func startAction(_ sender: Any) {
        Stopper.sharedInstance.start()
    }
    @IBAction func pauseAction(_ sender: Any) {
        Stopper.sharedInstance.pause()
    }
    
//    func updateLabel() {
//        label.text = String(seconds)
//    }
//
//    func useTimerMethod2() {
//        let mySecretData = ["a", "b", "c"]
//        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (senderTimer) in
//            self.seconds += 1
//            self.updateLabel()
//
//            print(mySecretData)
//
//            if(self.seconds == 3) {
//                senderTimer.invalidate()
//            }
//        }
//    }
//
//    func useTimerMethod1() {
//        let mySecretData = ["a", "b", "c"]
//        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(timerFired(senderTimer:)), userInfo: mySecretData, repeats: true)
//    }
//
//    @objc func timerFired(senderTimer: Timer) {
//        seconds += 1
//        updateLabel()
//
//        if(seconds == 10) {
//            senderTimer.invalidate()
//        }
//    }


}

