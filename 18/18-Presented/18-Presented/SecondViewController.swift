//
//  SecondViewController.swift
//  18-Presented
//
//  Created by Andras Somlai on 2018. 11. 20..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var secondLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(secondsChanged(senderNotification:)), name: Notification.Name("secondsChanged"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func secondsChanged(senderNotification: Notification) {
        print("userInfo: \(senderNotification.userInfo)")
        
        let info = senderNotification.userInfo as! [String : Bool]
        if(info["special"] == true) {
            view.backgroundColor = UIColor.green
        }
        else {
            view.backgroundColor = UIColor.white
        }
        updateLabel()
    }
    
    func updateLabel() {
        secondLabel.text = String(Stopper.sharedInstance.seconds)
    }

}
