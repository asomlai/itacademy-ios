//
//  Stopper.swift
//  18-Presented
//
//  Created by Andras Somlai on 2018. 11. 20..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Stopper {
    static let sharedInstance = Stopper()
    private init() {}
    
    var timer: Timer?
    var seconds = 0 {
        didSet {
            
            NotificationCenter.default.post(name: Notification.Name("secondsChanged"), object: nil, userInfo: ["special" : seconds%3 == 0])
        }
    }
    
    
    func start() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerFired(senderTimer:)), userInfo: nil, repeats: true)
    }
    
    func pause() {
        timer?.invalidate()
    }
    
    @objc func timerFired(senderTimer: Timer) {
        seconds += 1
    }

}
