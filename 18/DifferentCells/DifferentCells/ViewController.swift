//
//  ViewController.swift
//  DifferentCells
//
//  Created by Andras Somlai on 2018. 05. 17..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    private let actionNames = ["Start", "Pause", "Stop", "Restart"]

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + actionNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //0. sorban van a timer cell
        if(indexPath.row == 0) {
            return getTimerCell(indexPath: indexPath)
        }
        //A többi sorban vannak a gombok
        else if(indexPath.row > 0 && indexPath.row <= actionNames.count) {
            return getActionCell(indexPath: indexPath)
        }
        else {
            return getCarCell(indexPath: indexPath)
        }
    }
    
    func getTimerCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "timerCell", for: indexPath) as! TimerCell
        return cell
    }

    func getActionCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "actionCell", for: indexPath) as! ActionCell

        //A cella sorának megfelelő action string beállítása
        //indexPath.row-1-ik elem kell, mert a tableView elején van egy másik sor.
        cell.actionName = actionNames[indexPath.row-1]
        return cell
    }
    
    func getCarCell(indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "carCell", for: indexPath) as! CarCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 2 + actionNames.count - 1) {
            return 200
        }
        return 44
    }
}

