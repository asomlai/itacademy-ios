//
//  CustomTableViewCell.swift
//  TableViewCellDelegate
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

protocol CustomTableViewCellDelegate : class {
    func customCellLikePressed(cell: CustomTableViewCell)
    func customCellNeutralPressed(cell: CustomTableViewCell)
    func customCellDislikePressed(cell: CustomTableViewCell)
}

class CustomTableViewCell: UITableViewCell {
    weak var delegate : CustomTableViewCellDelegate?
    

    @IBAction func like(_ sender: UIButton) {
        delegate?.customCellLikePressed(cell: self)
    }
    
    @IBAction func neutral(_ sender: UIButton) {
        delegate?.customCellNeutralPressed(cell: self)
    }
    
    @IBAction func dislike(_ sender: UIButton) {
        delegate?.customCellDislikePressed(cell: self)
    }
}
