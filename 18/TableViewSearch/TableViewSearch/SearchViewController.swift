//
//  SearchViewControllerTableViewController.swift
//  TableViewSearch
//
//  Created by Andras Somlai on 2018. 11. 20..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

protocol SearchViewControllerDelegate {
    func itemSelected(item: String, dataType: DataType)
}

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var searchedDataType : DataType!
    var delegate : SearchViewControllerDelegate?
    var data : [String]!
    var defaultSelected : String?
    var visibleData : [String]!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        visibleData = data
        
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visibleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let actualElement = visibleData[indexPath.row]
        cell.textLabel?.text = actualElement
        
        if(actualElement == defaultSelected) {
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected = visibleData[indexPath.row]
        delegate?.itemSelected(item: selected, dataType: searchedDataType)
        navigationController?.popViewController(animated: true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        visibleData.removeAll()
        
        if(searchText == "") {
            visibleData = data
            tableView.reloadData()
            return
        }
        
        for item in data {
            if(item.lowercased().contains(searchText.lowercased())) {
                visibleData.append(item)
            }
        }
        tableView.reloadData()
    }
}
