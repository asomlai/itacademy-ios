//
//  ViewController.swift
//  TableViewSearch
//
//  Created by Andras Somlai on 2018. 11. 20..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum DataType {
    case producerData
    case modelData
}

class ViewController: UIViewController, SearchViewControllerDelegate {
    let producers: [String] = ["Alfa Romeo", "Suzuki", "BMW", "Renault", "Ferrari", "Bugatti", "Mitsubishi", "Lamborghini", "Honda", "Fiat", "Seat", "Skoda", "Subaru", "Ford", "Opel", "Mercedes-Benz", "Mercedes-AMG", "Toyota", "Hyuandi"]
    
    let models: [String] = ["A1", "B1", "C1", "D1", "A2", "B2", "C2", "D2", "AA1", "BC1", "CD1", "DE1", "AAA2", "CDB2", "CER2", "DASD2", "Opel"]
    
    var selectedProducer : String?
    var selectedModel : String?
    
    @IBOutlet weak var producerButton: UIButton!
    @IBOutlet weak var modelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateButtons()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func selectProducer(_ sender: Any) {
        let senderData: (DataType, [String]) = (.producerData, producers)
        performSegue(withIdentifier: "showSearchViewController", sender: senderData)
    }
    
    @IBAction func selectModel(_ sender: Any) {
        let senderData: (DataType, [String]) = (.modelData, models)
        performSegue(withIdentifier: "showSearchViewController", sender: senderData)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let searchVC = segue.destination as? SearchViewController,
           let searchData = sender as? (DataType, [String]) {
            //ABC szerint rendezve adjuk át a tömböt
            searchVC.data = searchData.1.sorted { $0 < $1 }
            
            //Csak simán átadjuk a tömböt
            searchVC.data = searchData.1
            searchVC.searchedDataType = searchData.0
            if(searchData.0 == .modelData) {
                searchVC.defaultSelected = selectedModel
            }
            else if(searchData.0 == .producerData) {
                searchVC.defaultSelected = selectedProducer
            }
            searchVC.delegate = self
        }
    }
    
    func updateButtons() {
        if let model = selectedModel {
            modelButton.setTitle("Selected model: \(model)", for: .normal)
        }
        else {
            modelButton.setTitle("Select model", for: .normal)
        }
        
        if let producer = selectedProducer {
            producerButton.setTitle("Selected producer: \(producer)", for: .normal)
        }
        else {
            producerButton.setTitle("Select producer", for: .normal)
        }
    }
    
    func itemSelected(item: String, dataType: DataType) {
        if(dataType == .producerData) {
            selectedProducer = item
        }
        else if(dataType == .modelData) {
            selectedModel = item
        }
        
        updateButtons()
        
        
    }
}

