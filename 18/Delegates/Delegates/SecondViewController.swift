//
//  SecondViewController.swift
//  Delegates
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

protocol SecondViewControllerDelegate {
    func textChanged(newText: String)
}

class SecondViewController: UIViewController, UITextViewDelegate {
    var delegate : SecondViewControllerDelegate?
    var initialText : String!
    @IBOutlet weak var textView : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        textView.text = initialText
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.delegate?.textChanged(newText: textView.text)
    }

}
