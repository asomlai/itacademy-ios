//Helper
var ErrorHandler  = require('../service/ErrorHandler');

//MIDDLEWARES
//Device
// var saveDeviceDataIfRequired = require("../middleware/device/saveDeviceDataIfRequired");

//Authentication
var checkSession = require("../middleware/authentication/checkSession");
// var createSession = require("../middleware/authentication/createSession");
// var invalidateSessionsForDevice = require("../middleware/authentication/invalidateSessionsForDevice");
// var isLoggedIn = require("../middleware/authentication/isLoggedIn");
//
// //User
// var checkIfUserAlreadyExists = require("../middleware/user/checkIfUserAlreadyExists");
// var registerUser = require("../middleware/user/registerUser");
// var loginUser = require("../middleware/user/loginUser");




module.exports = function (app) {

	app.delete('/book/:bookId',
    ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], []), //Optional: [][][]
    checkSession(),

	  function (req, res, next) {
			if(req.params.bookId == undefined) {
        res.sendError(404, "No book to delete")
      }
      else {
        res.send({"success" : true})
      }
		}
	)


	app.put('/book',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], ["book"]), //Optional: [][][]
		checkSession(),
	  function (req, res, next) {
			res.send({"success" : true})
		}
	)


	app.post('/book',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], ["book"]), //Optional: [][]['pushToken']
    checkSession(),
	  function (req, res, next) {
			res.send({"success" : true})
		}
	)


	app.get('/book/:bookId',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], []), //Optional: [][][]
		checkSession(),
	  function (req, res, next) {
			res.send({
        "id" : 2,
        "title" : "Ready player one",
        "isbn" : "gergr",
        "authors" : ["Ernest Cline"],
        "publisher" : "TheBook CO",
        "coverUrl" : "https://images-na.ssl-images-amazon.com/images/I/51N%2BZ39KtlL._SX314_BO1,204,203,200_.jpg",
        "rating" : 3
      });
		}
	)


  app.get('/book/',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], []), //Optional: [][][]
		checkSession(),
	  function (req, res, next) {
			res.send([
        {
          "id" : 1,
          "title" : "Winnie the Pooh",
          "isbn" : "dsadsad",
          "authors" : ["A. A. Milne", "E.H. Shepard"],
          "publisher" : "TheBook CO",
          "coverUrl" : "https://pictures.abebooks.com/isbn/9780749702106-uk-300.jpg",
          "rating" : 2
        },
        {
          "id" : 2,
          "title" : "Ready player one",
          "isbn" : "gergr",
          "authors" : ["Ernest Cline"],
          "publisher" : "TheBook CO",
          "coverUrl" : "https://images-na.ssl-images-amazon.com/images/I/51N%2BZ39KtlL._SX314_BO1,204,203,200_.jpg",
          "rating" : 3
        }
      ]);
		}
	)

}
