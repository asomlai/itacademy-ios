var common = require('../common');


function loginUser() {
  var userModel = common.requireObjectRepository('userModel');
	return function(req, res, next) {
		var email = req.body.email;
    var password = req.body.password;

    userModel.findOne({
			where: {
				email: email,
				deleted: false
			}
		}).then(user => {
      //If user not found with the given email
      if(user == undefined || user == {}) {
        res.sendError(404, "User with email not found");
        return;
      }

      //If found user oassword does not match to given password
			if (user.password != password) {
        res.sendError(401, "Wrong password");
        return;
			}

      //If everything is okay
      res.data.user = user;
      next();

		})
    .catch(err => {
			res.sendError(500, "DB error: "+err);
		})
	}
}

module.exports = loginUser;
