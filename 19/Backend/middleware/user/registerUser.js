var common = require('../common');


function registerUser() {
  var userModel = common.requireObjectRepository('userModel');
	return function(req, res, next) {
		var email = req.body.email;
    var password = req.body.password;
    var repassword = req.body.repassword;

    if(password != repassword) {
      res.sendError(400, "password and repassword are different");
      return;
    }

    //Create new user object
    var userObject =
    {
      email : email,
      password : password
    }

    //Save new user object
    userModel.create(userObject)
    .then(user => {

      //Is save wasn't successful
      if(user == undefined || user == {}) {
        res.sendError(503, "Cant create user object");
        return;
      }

      //If operation was successful
      res.data.user = user;
      next();
    })
    .catch(err => {
			res.sendError(503, "DB error: " + err);
      return;
		})
	}
}

module.exports = registerUser;
