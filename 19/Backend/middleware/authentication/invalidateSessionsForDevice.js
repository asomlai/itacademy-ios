var common = require('../common');



function invalidateSessionsForDevice() {
	var deviceModel = common.requireObjectRepository('deviceModel');
	var sessionModel = common.requireObjectRepository('sessionModel');

	return function(req, res, next) {

		//Get device with given hardwareId
		deviceModel.findOne({
			"where": {
				"hardwareId": req.get("hardwareId"),
				"deleted" : false
			}
		})
		.then(device => {
			//If no device was found, it can't have valid session, so we can go to next mw
			if (device == undefined) {
				next();
				return;
			}
			//If device was found, I have to update the deleted flag of it's session
			else {
				sessionModel.update({
					"deleted": true
				}, {
					fields: ['deleted'],
					where: {
						"deviceId": device.id
					}
				})
				.then(deletedSession => {
					next();
				})
			}
		})
		.catch(err => {
			res.sendError(503, "DB error: " + err)
		});
	}
}

module.exports = invalidateSessionsForDevice;
