//Helper
var ErrorHandler  = require('../service/ErrorHandler');

//MIDDLEWARES
//Device
//var registerDeviceToPush = require("../middleware/device/registerDeviceToPush");
var saveDeviceDataIfRequired = require("../middleware/device/saveDeviceDataIfRequired");

//Messaging
//var subscribeToPushChannel = require("../middleware/messaging/subscribeToPushChannel");

//Authentication
var checkSession = require("../middleware/authentication/checkSession");
var createSession = require("../middleware/authentication/createSession");
var invalidateSessionsForDevice = require("../middleware/authentication/invalidateSessionsForDevice");
var isLoggedIn = require("../middleware/authentication/isLoggedIn");

//User
var checkIfUserAlreadyExists = require("../middleware/user/checkIfUserAlreadyExists");
var registerUser = require("../middleware/user/registerUser");
var loginUser = require("../middleware/user/loginUser");




module.exports = function (app) {

/*
 * Belépteti a regisztrált felhasználót az alkalmazásba
 */
	app.post('/login',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'os', 'osVersion'], [], ['email', 'password']), //Optional: [][]['pushToken']
		invalidateSessionsForDevice(),
		loginUser(),
		saveDeviceDataIfRequired(),
		//registerDeviceToPush(),
		createSession(),
		//subscribeToPushChannel(),
	  function (req, res, next) {
			res.data.user.password = undefined
			res.send({sessionId : res.data.session.sessionId, user : res.data.user});
		}
	)

/*
 * Kilépteti a bejelentkezett felhasználót az alkalmazásból
 */
	app.post('/logout',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], []), //Optional: [][][]
		checkSession(),
		invalidateSessionsForDevice(),
	  function (req, res, next) {
			res.sendStatus(200);
		}
	)

/*
 * Regisztrálja és belépteti a regisztrált felhasználót az alkalmazásba
 */
	app.post('/register',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'os', 'osVersion'], [], ['email', 'password', 'repassword']), //Optional: [][]['pushToken']
		checkIfUserAlreadyExists(),
		registerUser(),
		saveDeviceDataIfRequired(),
		//registerDeviceToPush(),
		createSession(),
		//subscribeToPushChannel(),
	  function (req, res, next) {
			res.data.user.password = undefined
			res.send({sessionId : res.data.session.sessionId, user : res.data.user});
		}
	)

/*
 * Regisztrálja és belépteti a regisztrált felhasználót az alkalmazásba
 */
	app.get('/profile',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], []), //Optional: [][][]
		checkSession(),
	  function (req, res, next) {
			res.data.user.password = undefined;
			res.data.user.devices = undefined;
			res.send({user : res.data.user});
		}
	)

	app.get('/isLoggedIn',
		ErrorHandler.checkRequiredParameters(['hardwareId', 'sessionId'], [], []), //Optional: [][][]
		isLoggedIn()
	)

}
