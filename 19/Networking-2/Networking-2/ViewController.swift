//
//  ViewController.swift
//  Networking-2
//
//  Created by Andras Somlai
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //loginSuccess
    override func viewDidLoad() {
        super.viewDidLoad()
//        NetworkManager.getPeople { (peopleData, error) in
//            print(peopleData)
//        }
        
//        NetworkManager.getPeople { (data, error) in
//            print(data)
//        }
        
        //NetworkManager.getPeople(completion: processPeopleData(data:error:))
 
//        NetworkManager.login { (loginData, loginSuccess, error) in
//            print(loginData, loginSuccess, error)
//        }
//
//        NetworkManager.getClients { (clientData, error) in
//            //print(clientData)
//        }
//
//        NetworkManager.getMapData { (mapData, error) in
//            //print(mapData)
//        }
    }
    
//    func processPeopleData(data: [[String : Any]]?, error: Error?) {
//        print(data)
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login(_ sender: Any) {
        (sender as! UIButton).isEnabled = false
        
        //Ha a függvényt külön megírtam, úgy is átadhatom paraméterként
        NetworkManager.login(email: emailTextField.text!, password: passwordTextField.text!, completion: handleLoginResonse(loginData:loginSuccess:error:))
        
        //Vagy itt helyben megírhatum a logikát
        NetworkManager.login(email: emailTextField.text!, password: passwordTextField.text!) { (loginData, loginSuccess, error) in
            (sender as! UIButton).isEnabled = true
            if(loginSuccess == true) {
                self.performSegue(withIdentifier: "loginSuccess", sender: nil)
                print("navigáció, siker")
            }
            else {
                
                print("bejelentkezés sikertelen")
            }
        }
    }
    
    
    func handleLoginResonse(loginData : [String : Any]?, loginSuccess : Bool, error : Error?) {
        //Itt nem tudom letiltani a login buttont, mert nincs ilyen propertyje a viewcontrolernek
        //(sender as! UIButton).isEnabled = true
        if(loginSuccess == true) {
            self.performSegue(withIdentifier: "loginSuccess", sender: nil)
            print("navigáció, siker")
        }
        else {
            
            print("bejelentkezés sikertelen")
        }
    }
}

