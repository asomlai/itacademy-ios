//
//  NetworkManager.swift
//  Networking-2
//
//  Created by Andras Somlai
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {

    static func getPeople(completion : @escaping ([[String : Any]]?, Error?) -> Void) {
        
        Alamofire.request("http://142.93.200.209/itacademy/people").responseJSON { response in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? [[String : Any]] {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
    }
    
    static func login(email: String, //Ezzel az email címmel próbál meg belépni a user
                      password: String, //Ezzel a jelszóval próbál meg belépni a user
        completion : @escaping ([String : Any]?, Bool, Error?) -> Void) { //Ezt a függvényt fogja meghívni a program,, miután befejeződött a hűlózati kommunikáció. @escaping kulcsszó: A loginm fgv futásának befejezése után hívódik meg a cpmpletion paraméter függvény. Hálózati kommunikáció esetében mindig kell ez a kulcsszó a függvény paraméter elé.
        
        
        let loginParameters = ["email" : email, "password" : password] //Body paramétereket összerakjuk. Függvény paraméterek alapján
        let headerParameters = ["hardwareId" : "sssa", "os" : "ios", "osVersion" : "11.3.2"] //Header paramterket összerakjuk
        
        
        //http kérés elküldése
        Alamofire.request("http://142.93.200.209/itacademy/login", //url megadása
                          method: .post, //http metódus megadása
                          parameters: loginParameters, //body paraméterek
                          headers: headerParameters /*header parampéterek megadása*/).responseJSON { (response) in
                            //Ez a kódrészlet fut le, miután befejeződött a hálózati kommunikáció
            
                            let statusCode = response.response?.statusCode //response státusz küódjának lekérése
                            let dictionaryData = response.result.value as? [String : Any]  //response body castolása dictionaryvé
                            let arrayData = response.result.value as? [[String : Any]] //response body casoltása dictionaryk tömbjévé
                            let stringArrayData = response.result.value as? [String]
                            //A FENTI 3 közül csak egyet kell választanotok
                            
                            //Visszahívom azt a kódrészletet, amit ott adtam át paraméterül, ahol a NetworkManager.logint függvényt meghívtam
                            completion(dictionaryData, statusCode == 200, response.error)
        }

    }
    
    static func getClients(completion : @escaping ([String : Any]?, Error?) -> Void) {
        let header = ["token" : "itacademy"]
        Alamofire.request("http://142.93.200.209/itacademy/clients", headers: header).responseJSON { response in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? [String : Any] {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
    }
    
    static func getMapData(completion : @escaping ([String : Any]?, Error?) -> Void) {
        let urlParam = ["markersRequested" : true]
        Alamofire.request("http://142.93.200.209/itacademy/mapData", parameters: urlParam, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? [String : Any] {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
    }
    
    
}
