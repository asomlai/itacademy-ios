//
//  ViewController.swift
//  TextFieldUnderKeyboard
//
//  Created by Andras Somlai on 2018. 11. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textFieldBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.delegate = self
        
        //Így "iratkozol fel" arra, hogy hívódjon meg a keyboardWillAppear(notification:) függvény, amikor a billentyűzet megjelenik
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
    }
    
    
    //Mivel selector paraméterként lett átadva, oda kell írni a @objc prefixet a func elé
    //Ez a függvény hívódik meg amikor a billzet megjelenik
    @objc func keyboardWillAppear(notification: Notification) {
        // finalKeyboardFrame értéke lesz a billzet frame-je
        let finalKeyboardFrame = notification.userInfo!["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        
        textFieldBottomConstraint.constant = finalKeyboardFrame.height + 20
    }
    
    
    //Ha befejeződött a textfield szerkesztése, akkor ez a függvény hívódik meg
    func textFieldDidEndEditing(_ textField: UITextField) {
        textFieldBottomConstraint.constant = 100
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }


}

