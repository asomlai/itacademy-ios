//
//  ViewController.swift
//  15-Presenteds
//
//  Created by Andras Somlai on 2018. 11. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
    
    let myCustomView = Cubes()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        label.numberOfLines = 2
        label.lineBreakMode = .byWordWrapping
        
        label.text = "UILabel többsoros szöveggel\nUIImageView contentMode UIAlertViewUITextField keyboardType"
        
        
        textField.isSecureTextEntry = true
        
        myCustomView.frame = CGRect(x:0, y:0, width:100, height:100)
        view.addSubview(myCustomView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
    }

    
    @objc func keyboardWillAppear(notification: Notification) {
        //Do something here
        let finalKeyboardFrame = notification.userInfo!["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        
        
    }
    
    @IBAction func showAlert(_ sender: UIButton) {
        let alertView = UIAlertController(title: "UILabel többsoros szöveggel UILabel többsoros szöveggel", message: "UIImageView contentMode UIAlertViewUITextField keyboardType UIImageView contentMode UIAlertViewUITextField keyboardType", preferredStyle: .alert)
        
        let valtozo = "dasdsasad"
        
        
        let okAction1 = UIAlertAction(title: "secure", style: .destructive) { (result) in
            print(valtozo)
            self.textField.isSecureTextEntry = true
        }
        
        let okAction2 = UIAlertAction(title: "not secure", style: .cancel) { (result) in
            self.textField.isSecureTextEntry = false
        }
        
        let okAction3 = UIAlertAction(title: "kep nagyitas", style: .default) { (result) in
            self.imageViewHeight.constant = 300
            self.view.setNeedsLayout()
        }
        
        let okAction4 = UIAlertAction(title: "Title", style: .default) { (_) in
            
        }
        
        
        alertView.addAction(okAction1)
        alertView.addAction(okAction2)
        alertView.addAction(okAction3)
        alertView.addAction(okAction4)
        
        present(alertView, animated: true, completion: nil)
        
    }
    
}

