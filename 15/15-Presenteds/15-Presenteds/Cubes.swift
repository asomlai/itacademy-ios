//
//  Cubes.swift
//  15-Presenteds
//
//  Created by Andras Somlai on 2018. 11. 08..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Cubes: UIView {

    let redView = UIView()
    let blueView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    init() {
        super.init(frame: CGRect(x:0, y:0, width:0, height:0))
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    func setupUI() {
        addSubview(redView)
        addSubview(blueView)
        
        redView.backgroundColor = UIColor.red
        blueView.backgroundColor = UIColor.blue
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        redView.frame = CGRect(x:0, y:0, width:frame.width/3, height: frame.height)
        blueView.frame = CGRect(x:frame.width*2/3, y:0, width:frame.width/3, height: frame.height)
    }
    
}
