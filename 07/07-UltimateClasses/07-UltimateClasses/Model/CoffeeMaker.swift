//
//  CoffeeMachine.swift
//  06
//
//  Created by Andras Somlai on 2018. 04. 05..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Cocoa

class CoffeeMaker {
    static let standardCoffeeMakerPrice : Double = 15000 //A kávégépek ára
    
    var producerName : String //Gyárója
    var waterTankSize : Double  //Mekkora a víztartálya
    var color : String //Milyen színű
    var powerConsumption : Double //Mennyi energiára van szüksége 1 kávé elkészítéséhez
    var waterLevel : Double = 0 //Mennyi víz van beletöltve
    var water : Water? //Milyen víz van beletöltve
    var availableCoffeeSizes : [CoffeeSize] = [.small, .large] //Milyen méretű kávék főzésére alkalmas a gép
    
    
    init() {
        producerName = "CoffeMaker CO"
        waterTankSize = 1.5
        color = "black"
        powerConsumption = 220.0
    }
    
    init(factoryName : String) {
        producerName = factoryName
        waterTankSize = 1.5
        color = "black"
        powerConsumption = 220.0
    }
    
    init(factoryName : String, waterTank : Double, machineColor : String, consumptionOfPower : Double) {
        producerName = factoryName
        waterTankSize = waterTank
        color = machineColor
        powerConsumption = consumptionOfPower
    }
    
    func makeCoffee(power : Power, capsule : Capsule, size : CoffeeSize) -> Coffee? {
        if (power.availablePower < powerConsumption) {
            debugPrint("Nincs elegendő energia a kávéfőzéshez")
            return nil
        }
        
        if(water == nil) {
            debugPrint("Nincs víz a kávéfőzőben")
            return nil
        }
        
        if(waterLevel < Coffee.coffeeAmountFor(size: size)) { //Ha az adott méretű kávét nem lehet lefőzni
            debugPrint("Nem sikerült kávét főzni. Nincs elég víz a tartályban. Töltsd újra!")
            return nil
        }
        
        
        if (capsule.isUsed == true) {
            debugPrint("Használt kapszulából nem lehet kávét főzni")
            return nil
        }
        
        if(availableCoffeeSizes.contains(size) == false) { //Ha a lefőzni kívánt méretű kávé elkészítésére nem alkalmas a gépünk
            debugPrint("A gép csak\(getAvailableCoffeeSizesAsString()) méretű kávékat tud főzni")
            return nil
        }
        
        let coffee = Coffee(machine: self, water: water!, capsule: capsule) //Kávé elkészítése
        coffee.amount = Coffee.coffeeAmountFor(size: size) //Kávé méretének beállítása
        
        waterLevel -= coffee.amount!    //víztartályban lévő víz mennyisége csökkent
        
        power.availablePower -= powerConsumption //A paraméterként átadott energia csökkent
        capsule.isUsed = true //A paraméterként átadott kapszula elhasználódott
        
        //Ha a waterLevel 0, akkor nincs víz a kávéfőzőben
        //A self.water-t ennek megfelelően nil-re kell állítani
        //Azért így vizsgálom, mert a Double típus számábrázolási pontatlanságaiból eredően lehet, hogy logikusan 0 lenne az érték, de a változóban valami elhanyagolhatóan kicsi
        //de nullától eltérő érték van. Pl:  0.000000000000443
        if(waterLevel < 0.000001) {
            self.water = nil
        }
        
        return coffee
    }
    
    //Víztartály teletöltése
    func fillWaterTank(water : Water) {
        self.water = water
        waterLevel = waterTankSize
    }
    
    //Feltölti a víztartályt a paraméterül átadott vízzel, a paraméterül átadott mennyiségben
    func fillWaterTank(filledWaterQuantity : Double, filledWater : Water) {
        if(filledWaterQuantity + waterLevel > waterTankSize) { //Ha többet töltünk bele, mint ami belefér, kifolyik a víz
            debugPrint("Kifolyt a víz") //Ezt csak logoljuk
        }
        waterLevel = min(waterTankSize, filledWaterQuantity + waterLevel) //A víztartályt legfeljebb tele tölthetjük, emiatt szükséges a min függvény használata.
        water = filledWater //Lecseréljük a kávégépben lévő vizet az új vízre
    }
    
    //Visszaadja stringként a kávéfőző által lefőzhető kávék méreteit. Logoláshoz
    private func getAvailableCoffeeSizesAsString() -> String {
        var returnString = ""
        for actualCoffeeSize in availableCoffeeSizes {
            returnString = returnString + " " + actualCoffeeSize.rawValue
        }
        return returnString
    }
    
    func printCoffeeMakerDetails() {
        debugPrint("Víztartályban lévő víz: \(waterLevel)  --  víztartályban mérete:\(waterTankSize)")
    }
}
