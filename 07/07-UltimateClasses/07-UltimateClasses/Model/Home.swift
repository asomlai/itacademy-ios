//
//  Home.swift
//  06
//
//  Created by Andras Somlai on 2018. 04. 05..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Cocoa

class Home {
    var coffeeMaker : CoffeeMaker? {
        willSet {
            if(coffeeMaker != nil && newValue != nil) {
                debugPrint("A régi kávéfőzőt újra cserélték")
            }
        }
        didSet {
            //Ha a kávéfőző értéke nil (tehát pl ha kidobták a régit)
            if(coffeeMaker == nil) {
                //A családban mindenki szomorú lesz
                for human in people {
                    human.mood = .sad
                }
            }
            //Ha van új kávéfőző
            else {
                //Mindenki boldog lesz a családban ha lesz új kávéfőző
                for human in people {
                    human.mood = .happy
                }
                //Kávégéphez adnak 20 ingyen kapszulást
                for _ in stride(from: 0, to: 20, by: 1) {
                    capsules.append(Capsule())
                }
            }
        }
    }
    
    var address : String = "ismeretlen" //Az otthon címe
    var water : Water = Water() //Az otthonban lévő víz (tartályból jön, nem vezetékről --> eflogyhat)
    var power : Power = Power() //Az otthonban lévő energia (akkumulátorból jön, nem hálóüzatról --> elfogyhat)
    var availableWater : Double = 200 //Az otthon vítrartályának mérete
    var people : [Human] = [] //Az otthonban lakó emberek (avagy családtagok)
    var capsules : [Capsule] = [] //Az otthonban található kávékapszulák
    

    // A Home osztályba írjátok meg a coffeeParty függvényt, ami addig főzi a kávét, amíg van elég víz és áram a lakásban. Amint valamelyik kifogyott, írja ki a konzolra, hogy mi fogyott el
    // Ez még az előző órához tartozó feladat
    func coffeeParty() {
        //Ha nincs kávégép, a függvény hibát ír a konzolra, és befejezi a futását a return paranccsal
        if(coffeeMaker == nil) {
            debugPrint("Kávégép nélkül nem lehet kávét főzni")
            return
        }
        
        //A lefőzött kávék számát ebben fogom tárolni
        var numberOfCookedCoffees = 0
        
        //Végtelen ciklus, de breakkel majd kilépek a törzsében
        while (true) {
            let coffeeCapsule = Capsule() //Feltétlezem, hogy végtelen számú kávékapszula áll rendelkezésre
            let coffee = coffeeMaker?.makeCoffee(power: self.power, capsule: coffeeCapsule, size: .large) //Megpróbálok egy új kávét készíteni
            
            
            if(coffee == nil) { //Ha nem sikerült a kávé elkészítése, akkor nil lesz az új kávé értéke
                if(power.availablePower < coffeeMaker!.powerConsumption) { //Ha elfogyott az energia a lakásból, akkor a ciklus véget ér, nem lehet több kávét főzni
                    break //Kilépés a ciklusból
                }
                else if(availableWater == 0) { //Ha elfogyott a víz a lakásból, a ciklus véget ér
                    debugPrint("Elfogyott a lakásból a víz")
                    break //Kilépés a ciklusból
                }
                else { //Ha van még energia és víz a lakásban, akkor a kávégépből fogyott ki a víz
                    let waterInCoffeeMaker = coffeeMaker!.waterLevel //Megnézem, hogy jelenleg mennyi víz van a kávégépben
                    let requiredWaterToFullFillTank = coffeeMaker!.waterTankSize - waterInCoffeeMaker //Kiszámolom, hogy mennyi víz kell a kávégép víztartályának teletöltéséhez
                    let filledWater = min(requiredWaterToFullFillTank, availableWater) //Ennyi vizet fogok ténylegesen betölteni a kávégépben. Ha kevesebb víz van a lakásban, mint a mi a teletöltéshez kell, értelemszerűen nem tölhetem tele. Ezért van szükség erre a változóra

                    coffeeMaker!.fillWaterTank(filledWaterQuantity: filledWater, filledWater: self.water) //Beletöltöm a kávégépbe a vizet
                    availableWater -= filledWater //Csökkentem a lakásban elérhető vízt mennyiségét a kávégépbe töltött víz mennyiségével
                    debugPrint("feltöltöttem a kávégépet \(filledWater) liter vízzel")
                }
                
            }
            numberOfCookedCoffees += 1 //Növelem a lefőzött kávék számát
            print("Lefőzött kávék száma:\(numberOfCookedCoffees). Még ennyi víz van a lakásban: \(availableWater). Még ennyi áram van a lakásban:\(self.power.availablePower). Ennyi víz a kávégépben:\(coffeeMaker!.waterLevel)")
        }
    }
    
    //Új lakó hozzáadása
    func newPersonMovedToHere(person : Human) {
        people.append(person) //Hozzáadom a tömbhöz az új embert
        person.home = self  //Az embernek beállítom, hogy mostantól itt fog lakni
        debugPrint("\(person.name) új lakcíme: \(self.address)")
    }
    
    
    //A lakásban lévők statisztikáinak lekérdezése nemekre és munkákra vonatkozóan. Melyik nemből hány százalék van, illetve milyen arányban oszlanak meg az állások az itt lakók között
    //example return value:
    // 1 család lakik a házban, apuka programozó, felesége nem programozó, és van 2 lányuk
    // ["gender" : ["male" : 0.25, "female" : 0.75, "unknown" : 0], "job" : ["programmer" : 0.25, "child" : 0.5, "unknown" : 0.25]]
    func getDemographyStatisctics() -> [String : [String : Double]] {
        
        var numberOfMales = 0   //Ebben fogom tárolni a férfiak számát
        var numberOfFemales = 0 //Ebben fogom tárolni a nők számát
        var numberOfPeopleWithUnknownGender = 0  //Ebben fogom tárolni az ismeretlen neműek számát
        
        var numberOfChildren = 0 //Ebben fogom tárolni a gyerekek számát
        var numberOfProgrammers = 0  //Ebben fogom tárolni a programozók számát
        var numberOfPeopleWithUnknownJob = 0 //Ebben fogom tárolni az ismeretlen munkát végzők számát
        
        for human in people { //Végigiterálok a lakásban lévő embereken
            
            switch human.gender {   //Megvizsgálom az aktuális ember nemét, és növelem a megfelelő változó értékét
            case .male: numberOfMales += 1
            case .female: numberOfFemales += 1
            case .unknown: numberOfPeopleWithUnknownGender += 1
            }
            
            if(human is Programmer) {       //Megvizsgálom, hogy az aktuális ember programozó-e
                numberOfProgrammers += 1    //Ha programozó, növelem a programozók számát eggyel
            }
            else if(human is Child) {       //Megvizsgálom, hogy az aktuális ember gyerek-e
                numberOfChildren += 1       //Ha gyerek, növelem a gyerekek számát eggyel
            }
            else {
                numberOfPeopleWithUnknownJob += 1 //Egyéb esetben növelem az ismeretlen állású emberek számát
            }
        }
        
        //Létrehozom az elvárt típusú visszatérési értéket a megfelelő adatokkal
        let statistics = ["gender" : ["male" : Double(numberOfMales)/Double(people.count),
                                      "female" :  Double(numberOfFemales)/Double(people.count),
                                      "unknown" : Double(numberOfPeopleWithUnknownGender)/Double(people.count)],
                          "job" : ["programmer" :  Double(numberOfProgrammers)/Double(people.count),
                                   "child" :  Double(numberOfChildren)/Double(people.count),
                                   "unknown" :  Double(numberOfPeopleWithUnknownJob)/Double(people.count)]]
        return statistics
    }
    
    //Lakásban lévő adott ízű kapszulát adja vissza
    //Ha nincs olyan ízű kapszula, nillel tér vissza
    func getCapsule(taste : CoffeeTaste) -> Capsule? {
        for capsule in capsules { //Végigiterálok a lakásban lévő kapszulákon
            if(capsule.taste == taste && capsule.isUsed == false) {     //Ha van nem használt, és a paraméternek megfelelő ízű kapszula
                return capsule  //Visszaadja a függvény
            }
        }
        debugPrint("Nincs \(taste.rawValue) ízű kapszula")
        return nil //Ha a ciklus lefutott, és nem hívodot meg a törzsében a return --> Nincs megfelelő kapszula, tehát nil-lel tér vissza a függvény
    }
    
    //A hazsnált kapszulák kidobása
    func throwOutUsedCapsules() {
        var notUsedCapsules : [Capsule] = [] //Ebben a tömbben fogom tárolni a még nem használt kapszulákat
        for capsule in capsules {   //Végigiterálok a kapszulákon
            if(capsule.isUsed == false) {   //Ha a kapszula nem használt
                notUsedCapsules.append(capsule) //Hozzáadom a nem használt kapszulák tömbhöz
            }
        }
        self.capsules.removeAll() //Eltávolítm a capsules elemeit
        self.capsules.append(contentsOf: notUsedCapsules) //Hozzáadom a nem használt kapszulákat a capsuleshez
    }
    
    //Új kapszulát vásárolt valaki a lakásba
    func addNewCapcules(capsulesToAdd : [Capsule]) {
        var newCapsuleTastes : Set<CoffeeTaste> = Set() //A vásárolt kapszulák ízeit ebben a setben fogom tárolni
        for capsule in capsulesToAdd {  //végigiterálok a frissen vásárolt kapszulákon
            newCapsuleTastes.insert(capsule.taste) //Feltöltöm a setet a frisen vásárolt kapszulák ízeivel
        }
        for human in people { //Végigiterálok a lakásban lévő embereken
            if(newCapsuleTastes.contains(human.favouriteCoffeeTaste) == true) { //Ha az új kapszulák között van olyan ízű, ami az adott ember kedvenc íze, akkor boldog lesz
                human.mood = .happy
            }
            else {  //Ha az ő kedvenc ízéből nem vettek, akkor mérges lesz
                human.mood = .angry
            }
        }
        
        self.capsules.append(contentsOf: capsulesToAdd)  //Hozzáadom a lakásban lévő kapszulákhoz a frissen vásároltakat
    }
    
    //Csak logolás
    func printFamilyData() {
        for human in people {
            debugPrint("Lakó neve: \(human.name)  --  Lakó Id-je:\(human.id)  --  Lakó kedve:\(human.mood.rawValue)  --  Lakó pénze:\(human.money)  --  Lakó kávépénze:\(human.moneyForCoffee)  --  Lakó anyja neve:\(human.mother?.name)  --  Lakó apja neve:\(human.father?.name)")
        }
    }
    
    //Csak logolás
    func printCapsuleData() {
        var usedCapsules = 0
        for capsule in capsules {
            if (capsule.isUsed == true) {
                usedCapsules += 1
            }
        }
        debugPrint("A lakásban \(capsules.count) kapszula van, ebből \(usedCapsules) használt")
    }
}
