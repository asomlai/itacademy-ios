//
//  Laptop.swift
//  07-Presented2
//
//  Created by Andras Somlai on 2018. 10. 04..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Laptop: Szamitogep {
    var kepernyopmeret: Double = 13
    var trackpadMeret: Double = 4
    
    override func bekapcsol() {
        print("Laptop - bekapcsol()")
        print("Lassan bootol")
        super.bekapcsol()
    }
}
