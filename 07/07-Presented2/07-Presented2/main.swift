//
//  main.swift
//  07-Presented2
//
//  Created by Andras Somlai on 2018. 10. 04..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Foundation

var t1 = Targy()
t1.megveszik(tulajNeve: "Helga")
t1.elvesztik()

var szamitogep = Szamitogep()
szamitogep.megveszik(tulajNeve: "Helga")
szamitogep.elvesztik()


print("program vége")

var laptop = Laptop()

//Tárgyból örökölve
print(laptop.pozicio)
print(laptop.terfogat)
print(laptop.tomeg)

//Számítógépből örökölve
print(laptop.memoria)
print(laptop.gyarto)

//Laptop propertije
print(laptop.kepernyopmeret)
print(laptop.trackpadMeret)

laptop.megveszik(tulajNeve: "Péter")
laptop.elvesztik()

laptop.bekapcsol()
print("Laptop be van kapcsolva? - \(laptop.beVanEKapcsolva)")


let pc = AsztaliSzamitogep()
pc.bekapcsol()
print("PC be van kapcsolva? - \(pc.beVanEKapcsolva)")
//print()
//print()
//print()
