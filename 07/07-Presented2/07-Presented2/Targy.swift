//
//  Targy.swift
//  07-Presented2
//
//  Created by Andras Somlai on 2018. 10. 04..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Targy {
    var terfogat : Double = 0
    var tomeg : Double = 0
    var pozicio : (x: Double, y: Double, z: Double) = (x:0, y:0, z:0)
    
    var tulajdonos : String?
    
    func megveszik(tulajNeve: String) {
        print("\(tulajNeve) megvette ezt a tárgyat")
        tulajdonos = tulajNeve
    }
    
    func elvesztik() {
        tulajdonos = nil
    }
}
