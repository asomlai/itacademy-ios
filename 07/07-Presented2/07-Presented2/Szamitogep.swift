//
//  Szamitogep.swift
//  07-Presented2
//
//  Created by Andras Somlai on 2018. 10. 04..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Szamitogep: Targy {
    var gyarto: String = "Apple"
    var memoria: Int = 8
    var hattertar: Int = 256
    var orajel: Double = 2.7
    var beVanEKapcsolva: Bool = false
    
    override func megveszik(tulajNeve: String) {
        print("\(tulajNeve) megvette ezt a számítógépet")
        tulajdonos = tulajNeve
    }
    
    override func elvesztik() {
        super.elvesztik()
        print("A volt tulaj sokat vesztett")
    }
    
    func bekapcsol() {
        print("Szamitogep - bekapcsol()")
        beVanEKapcsolva = true
    }
}
