//
//  Capsule.swift
//  07-Presented
//
//  Created by Andras Somlai on 2018. 10. 04..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class Capsule {
    var caffeine : Double
    var taste : String
    var color : String
    var producer : String
    var isUsed : Bool = false
    var promotionText : String?
    var shop : String?
    
    init(newCaffeine : Double, newTaste: String, newColor: String, newProducer : String) {
        caffeine = newCaffeine
        
        taste = newTaste
        
        color = newColor
        
        producer = newProducer
    }
    
    init(newCaffeine : Double, newProducer : String) {
        caffeine = newCaffeine
        producer = newProducer
        
        taste = "vanilla"
        color = "yellow"
        
        isUsed = true
    }
    
    
    func printShop(shopName: String) {
        print("boltnév: \(shopName)")
    }
}
