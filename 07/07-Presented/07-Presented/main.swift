//
//  main.swift
//  07-Presented
//
//  Created by Andras Somlai on 2018. 10. 04..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Foundation


let c1 = Capsule(newCaffeine: 8, newTaste: "Caramel", newColor: "brown", newProducer: "Nespresso")
print("c1 használt? - \(c1.isUsed)")


let c2 = Capsule(newCaffeine: 3, newProducer: "Nespresso")
print("c2 használt? - \(c2.isUsed), boltja: \(c2.shop)")
c2.shop = "Budapest, WestEnd"
//c2.shop = nil


c2.printShop(shopName: c2.shop ?? "ISMERETLEN")

c2.shop = nil
c2.printShop(shopName: c2.shop ?? "ISMERETLEN")

if(c2.shop != nil) {
    c2.printShop(shopName: c2.shop!)
}
else {
    c2.printShop(shopName: "ISMERETLEN")
}

//if let c2ShopName = c2.shop {
//    c2.printShop(shopName: c2ShopName)
//}


