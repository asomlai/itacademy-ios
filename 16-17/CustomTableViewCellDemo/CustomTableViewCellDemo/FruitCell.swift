//
//  FruitCell.swift
//  CustomTableViewCellDemo
//
//  Created by Somlai András on 2018. 11. 13..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

protocol FruitCellDelegate {
    func fruitQuantityUpdated(fruitName: String, newQuantity: Int)
}

class FruitCell: UITableViewCell {
    var delegate: FruitCellDelegate?
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var fruitNameLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    
    @IBAction func stepperChanged(_ sender: UIStepper) {
        countLabel.text = String(Int(sender.value)) + " db"
        delegate?.fruitQuantityUpdated(fruitName: fruitNameLabel.text!, newQuantity: Int(sender.value))
    }
    

}
