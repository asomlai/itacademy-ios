//
//  ViewController.swift
//  CustomTableViewCellDemo
//
//  Created by Somlai András on 2018. 11. 13..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, FruitCellDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var data : [(fruitName: String, count: Int)] = [("alma", 0), ("körte", 0), ("meggy", 0), ("citrom", 0), ("banán", 0), ("cseresznye", 0), ("őszibarack", 0), ("ananász", 0), ("szőlő", 0), ("komló", 0), ("szilva", 0), ("kókusz", 0)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("cellForRowAt indexPath")
        let cell = tableView.dequeueReusableCell(withIdentifier: "fruitCell", for: indexPath) as! FruitCell
        cell.countLabel.text = String(data[indexPath.row].count) + " db"
        cell.fruitNameLabel.text = data[indexPath.row].fruitName
        cell.stepper.value = Double(data[indexPath.row].count)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func fruitQuantityUpdated(fruitName: String, newQuantity: Int) {
        for i in stride(from: 0, through: data.count-1, by: 1) {
            if(data[i].fruitName == fruitName) {
                data[i].count = newQuantity
            }
        }
    }

}

