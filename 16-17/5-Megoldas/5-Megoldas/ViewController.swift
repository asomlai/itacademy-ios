//
//  ViewController.swift
//  5-Megoldas
//
//  Created by Andras Somlai on 2018. 11. 15..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        
        let numberToShow = (indexPath.row + 1) + (indexPath.section * 100)
        
        cell.textLabel?.text = String(numberToShow)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(section)"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        let numberToShow = (indexPath.row + 1) + (indexPath.section * 100)
        let alert = UIAlertController(title: "sor: \(numberToShow), szekció: \(indexPath.section)", message: "message", preferredStyle: .alert)
        
        
        
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        let notSureAction = UIAlertAction(title: "Not sure", style: .destructive) { (_) in
            let alert2 = UIAlertController(title: "title2", message: nil, preferredStyle: .alert)
            let cancelAction2 = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
            alert2.addAction(cancelAction2)
            
            self.present(alert2, animated: true, completion: nil)
        }
        
        
        let notSureAction2 = UIAlertAction(title: "____", style: .destructive) { (alerttt) in
            print("handler param: \(alerttt.title!)")
            self.myTableView.reloadData()
        }
        
        alert.addAction(cancelAction)
        alert.addAction(notSureAction)
        alert.addAction(notSureAction2)
        
        present(alert, animated: true, completion: nil)
        
    }
    
}


