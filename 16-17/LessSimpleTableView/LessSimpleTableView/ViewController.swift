//
//  ViewController.swift
//  LessSimpleTableView
//
//  Created by Andras Somlai
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: - Property list
    @IBOutlet weak var numberOfSectionsLabel: UILabel!
    @IBOutlet weak var numberOfRowsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var numberOfRowsInSection = 1
    var numberOfSections = 1
    let myTableHeaderView = UIView()
    let myTableFooterView = UIView()
    let alert = UIAlertController(title: "A nagy testvér mindent lát!", message: nil, preferredStyle: .alert)
    
    //MARK: - UIViewController default functions
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        myTableHeaderView.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        myTableFooterView.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        
        tableView.tableHeaderView = myTableHeaderView
        tableView.tableFooterView = myTableFooterView
        
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okAction)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.tableHeaderView!.frame = CGRect(x:0,
                                                  y:0,
                                                  width:tableView.frame.width,
                                                  height:30)
        
        tableView.tableFooterView!.frame = CGRect(x:tableView.tableFooterView!.frame.minX,
                                                  y:tableView.tableFooterView!.frame.minY,
                                                  width:tableView.frame.width,
                                                  height:10)
    }
    
    //MARK: - Storyboard Action
    @IBAction func dataChanged(_ sender: UIStepper) {
        switch sender.tag {
        case 1: numberOfSections = Int(sender.value)
        case 2: numberOfRowsInSection = Int(sender.value)
        default: break
        }
        
        numberOfRowsLabel.text = "Sorok száma 1 szekcióban: \(numberOfRowsInSection)"
        numberOfSectionsLabel.text = "Szekciók száma: \(numberOfSections)"
        
        tableView.reloadData()
    }
    
    //MARK: - TableView DataSources
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath)
        cell.backgroundColor = UIColor.lightGray
        cell.textLabel?.text = "sor: \(indexPath.row)--szekció: \(indexPath.section)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Szekció-\(section) - HEADER"
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Szekció-\(section) - FOOTER"
    }
    
    //MARK: - TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Rányomtál egy sorra",
                                      message: "Az \(indexPath.section). szekció \(indexPath.row)-adik sorára",
                                    preferredStyle: .alert)
        
        let trueAction = UIAlertAction(title: "Ez valóban így történt", style: .default, handler: nil)
        let falseAction = UIAlertAction(title: "Nem nyomtam semmire", style: .default, handler:
        { action in
            debugPrint(action.title)
            self.showOtherAlert()
        })
        let closeAction = UIAlertAction(title: "Csak bezárnám a panelt", style: .destructive, handler: nil)
        
        alert.addAction(trueAction)
        alert.addAction(falseAction)
        alert.addAction(closeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Other functions
    func showOtherAlert() {
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(timerFired), userInfo: nil, repeats: false)
    }
    
    @objc func timerFired() {
        alert.dismiss(animated: true, completion: nil)
    }
 
}

