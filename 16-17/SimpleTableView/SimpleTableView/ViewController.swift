//
//  ViewController.swift
//  SimpleTableView
//
//  Created by Andras Somlai
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var data : [Bool] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for _ in stride(from: 0, to: 100, by: 1) {
            data.append(false)
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        //Alsó csíkok eltűntetésére hack
        tableView.tableFooterView = UIView()
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("sor: \(indexPath.row), szekció: \(indexPath.section)")
        print("CELL FOR ROW AT: \(indexPath)")
        
        let cellBoolean = data[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellaAzonosito", for: indexPath)
        cell.textLabel?.text = "sor: \(indexPath.row)--szekció: \(indexPath.section)"
        cell.textLabel?.backgroundColor = UIColor.clear
        cell.backgroundColor = cell.contentView.backgroundColor
        cell.selectionStyle = .none
        
        if(cellBoolean == true) {
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "szekció\(section)"
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Section Footer \(section)"
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Kiválasztott sor: \(indexPath)")
        
        data[indexPath.row] = !data[indexPath.row]
        tableView.reloadData()
//        let cell = tableView.cellForRow(at: indexPath)!
//        if(cell.accessoryType == .checkmark) {
//            cell.accessoryType = .none
//        }
//        else {
//            cell.accessoryType = .checkmark
//        }
    }
}

