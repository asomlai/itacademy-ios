//
//  ViewController.swift
//  2-Megoldas
//
//  Created by Andras Somlai on 2018. 11. 15..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.magenta
        
        tableView.separatorInset = .zero
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 110
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = String(indexPath.row)
        let newColor = UIColor(red: CGFloat(Double.random(in: 0...1)),
                               green: CGFloat(Double.random(in: 0...1)),
                               blue: CGFloat(Double.random(in: 0...1)),
                               alpha: 1)
        
        cell.textLabel?.textColor = newColor
        
        if(indexPath.row == 4) {
            cell.accessoryType = .checkmark
        }
        else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}

