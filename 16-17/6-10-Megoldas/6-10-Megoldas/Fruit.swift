//
//  Fruit.swift
//  6-10-Megoldas
//
//  Created by Andras Somlai on 2018. 11. 15..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Fruit {
    var name: String
    var description: String
    var quantity: Int
    var imageName: String
    
    init(newName: String, imageName: String) {
        self.name = newName
        self.imageName = imageName
        self.quantity = 0
        self.description = "Minions ipsum gelatooo butt belloo! Tank yuuu! Underweaaar. Gelatooo hahaha bananaaaa ti aamoo!"
    }
    
    init(newName: String) {
        self.name = newName
        self.imageName = newName.lowercased()
        self.quantity = 0
        self.description = "Minions ipsum gelatooo butt belloo! Tank yuuu! Underweaaar. Gelatooo hahaha bananaaaa ti aamoo!"
    }
    
    
}
