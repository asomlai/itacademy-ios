//
//  ViewController.swift
//  6-10-Megoldas
//
//  Created by Andras Somlai on 2018. 11. 15..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FruitCellDelegate {
    
    
    
    
    
    let fruits : [Fruit] = [Fruit(newName: "Apple"),
                            Fruit(newName: "Broccoli"),
                            Fruit(newName: "Carrot"),
                            Fruit(newName: "Cherries"),
                            Fruit(newName: "Cucumber"),
                            Fruit(newName: "Lemon"),
                            Fruit(newName: "Orange"),
                            Fruit(newName: "Pear"),
                            Fruit(newName: "Pineapple"),
                            Fruit(newName: "Pumpkin"),
                            Fruit(newName: "Strawberry"),
                            Fruit(newName: "WaterMelon")]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sumQuantityLabel: UILabel!
    @IBOutlet weak var sumContainerHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateSumQuantityLabel(animated: false)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fruits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FruitCell
        cell.setFruit(fruits[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    
    func quantityChanged(at cell: FruitCell, newValue: Int) {
        
        if let editedIndexPath = tableView.indexPath(for: cell) {
            print("quantityChanged at: \(editedIndexPath)")
            fruits[editedIndexPath.row].quantity = newValue
            updateSumQuantityLabel(animated: true)
        }
    }
    
    func updateSumQuantityLabel(animated: Bool) {
        var sum = 0
        for fruit in fruits {
            sum += fruit.quantity
        }
        
        if(sum == 0) {
            sumContainerHeight.constant = 0
            sumQuantityLabel.text = ""
        }
        else {
            sumContainerHeight.constant = 100
            sumQuantityLabel.text = String(sum) + " db"
        }
        
        if(animated == true) {
            view.setNeedsLayout()
            UIView.animate(withDuration: 1) {
                self.view.layoutIfNeeded()
            }
        }
        
        
    }
    

}

