//
//  FruitCell.swift
//  6-10-Megoldas
//
//  Created by Andras Somlai on 2018. 11. 15..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

protocol FruitCellDelegate {
    func quantityChanged(at cell: FruitCell, newValue: Int)
}

class FruitCell: UITableViewCell {
    var delegate: FruitCellDelegate?
    
    @IBOutlet weak var fruitNameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var fruitDescriptionLabel: UILabel!
    @IBOutlet weak var fruitImage: UIImageView!
    @IBOutlet weak var quantityStepper: UIStepper!
    
    @IBAction func quantityChanged(_ sender: UIStepper) {
        quantityLabel.text = String(Int(sender.value)) + " db"
        delegate?.quantityChanged(at: self, newValue: Int(sender.value))
    }
    
    func setFruit(_ fruit: Fruit) {
        fruitNameLabel.text = fruit.name
        fruitDescriptionLabel.text = fruit.description
        quantityLabel.text = String(fruit.quantity) + " db"
        quantityStepper.value = Double(fruit.quantity)
        fruitImage.image = UIImage(named: fruit.imageName)
    }
}
