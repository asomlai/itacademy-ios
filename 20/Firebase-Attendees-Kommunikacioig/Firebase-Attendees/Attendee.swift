//
//  Attendee.swift
//  Firebase-Attendees
//
//  Created by Andras Somlai on 2018. 11. 27..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum Gender : String {
    case male = "male"
    case female = "female"
}

class Attendee {
    var firstName : String
    var lastName : String
    var age : Int
    var id : Int
    var gender : Gender
    
    
    
    init(dictionary : [String : Any]) {
        let name = dictionary["name"] as! [String : String]
        firstName = name["firstname"]!
        lastName = name["lastname"]!
        age = dictionary["age"] as! Int
        id = dictionary["id"] as! Int
        let newGender = dictionary["gender"] as! String
        gender = Gender(rawValue: newGender)!
    }
    
    
    func printState() {
        print("""
            name: \(firstName) \(lastName)
            age: \(age)
            id: \(id)
            gender: \(gender.rawValue)
            ----------------
            
        """)
    }
}
