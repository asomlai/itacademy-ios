//
//  ViewController.swift
//  Firebase-Attendees
//
//  Created by Andras Somlai on 2018. 11. 27..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit



class ViewController: UIViewController, DataCenterDelegate {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //NotificationCenteres megoldáshoz
        //let dc1 = DataCenter.sharedInstance
        //dc1.getDataWithNotification()
        
        //Delegatees megoldáshoz
        //let dc2 = DataCenter.sharedInstance
        //dc2.delegate = self
        //dc2.getDataWithDelegate()
        
        //Completionös megoldáshoz 1
        //let dc3 = DataCenter.sharedInstance
        //dc3.getDataWithCompletion(completion: dataDownloadFinishedCompletion)
        
        //Completionös megoldáshoz 2
        let dc3 = DataCenter.sharedInstance
        dc3.getDataWithCompletion {
            print(dc3.attendees)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //Feliratkozás
        //NotificationCenteres megoldáshoz
        NotificationCenter.default.addObserver(self, selector: #selector(downloadFinished), name: Notification.Name("getData_done"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //Leiratkozás
        //NotificationCenteres megoldáshoz
        NotificationCenter.default.removeObserver(self)
    }
    
    //NotificationCenteres megoldáshoz
    @objc func downloadFinished() {
        let dc1 = DataCenter.sharedInstance
        print(dc1.attendees)
    }

    //Delegate-es megoldáshoz
    func dataCenterFinishedDownloading() {
        let dc1 = DataCenter.sharedInstance
        print(dc1.attendees)
    }
    
    //completionös megoldáshoz
    func dataDownloadFinishedCompletion() {
        let dc1 = DataCenter.sharedInstance
        print(dc1.attendees)
    }

}

