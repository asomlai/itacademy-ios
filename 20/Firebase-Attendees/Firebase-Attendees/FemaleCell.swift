//
//  FemaleCell.swift
//  Firebase-Attendees
//
//  Created by Andras Somlai on 2018. 11. 27..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class FemaleCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    
    func setAttendee(_ attendee: Attendee) {
        nameLabel.text = attendee.name
        iconImageView.image = attendee.ageIcon
    }

}
