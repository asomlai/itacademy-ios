//
//  StatsViewController.swift
//  Firebase-Attendees
//
//  Created by Andras Somlai on 2018. 11. 27..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class StatsViewController: UIViewController {

    @IBOutlet weak var attendeesCountLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let attendeesCount = DataCenter.sharedInstance.attendees.count
        attendeesCountLabel.text = "Résztvevők száma: \(attendeesCount)"
    }


}
