//
//  ViewController.swift
//  Firebase-Attendees
//
//  Created by Andras Somlai on 2018. 11. 27..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        DataCenter.sharedInstance.getDataWithCompletion {
            self.tableView.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataCenter.sharedInstance.attendees.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let actualAttendee = DataCenter.sharedInstance.attendees[indexPath.row]
        
        if(actualAttendee.gender == .female) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "femaleCell", for: indexPath) as! FemaleCell
            cell.setAttendee(actualAttendee)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "maleCell", for: indexPath) as! MaleCell
            cell.setAttendee(attendee: actualAttendee)
            return cell
        }
        
        
        
    }
   
}

