//
//  DataCenter.swift
//  Firebase-Attendees
//
//  Created by Andras Somlai on 2018. 11. 27..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase


//Delegate-es megoldáshoz
protocol DataCenterDelegate {
    func dataCenterFinishedDownloading()
}

class DataCenter {
    //Singleton
    static let sharedInstance = DataCenter()
    private init(){}
    
    var attendees : [Attendee] = []
    
    var delegate: DataCenterDelegate?
    
    
    //NotificationCenteres megoldáshoz
//    func getDataWithNotification() {
//        let ref = Database.database().reference()
//
//        ref.child("/").observeSingleEvent(of: .value, with: { (snapshot) in
//            if let data = snapshot.value as? [[String : Any]] {
//
//                for actualDict in data {
//                    let newAttendee = Attendee(dictionary: actualDict)
//                    self.attendees.append(newAttendee)
//                }
//                NotificationCenter.default.post(name: Notification.Name("getData_done"), object: nil)
//                //for i in stride(from: 0, to: data.count, by: 1) {
//                //   let actualDict = data[i]
//                //}
//            }
//        }) { (error) in
//            print(error.localizedDescription)
//        }
//    }
    
    
    //Delegatees megoildáshoz
//    func getDataWithDelegate() {
//        let ref = Database.database().reference()
//
//        ref.child("/").observeSingleEvent(of: .value, with: { (snapshot) in
//            if let data = snapshot.value as? [[String : Any]] {
//
//                for actualDict in data {
//                    let newAttendee = Attendee(dictionary: actualDict)
//                    self.attendees.append(newAttendee)
//                }
//
//                //Delegate-es megoldáshoz
//                self.delegate?.dataCenterFinishedDownloading()
//            }
//        }) { (error) in
//            print(error.localizedDescription)
//        }
//    }
    
    //Completionös megoldáshoz
    func getDataWithCompletion(completion: @escaping (() -> Void)) {
        let ref = Database.database().reference()
        
        ref.child("/").observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? [[String : Any]] {
                
                for actualDict in data {
                    let newAttendee = Attendee(dictionary: actualDict)
                    self.attendees.append(newAttendee)
                }
                
                //Tömb rendezése
                self.attendees.sort {
                    $0.name < $1.name
                }
                
                //Completion-ös megoldáshoz, függvény paraméter meghívása
                completion()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
}
